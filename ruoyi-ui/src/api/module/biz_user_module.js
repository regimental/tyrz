import request from '@/utils/request'

// 查询模块用户列表
export function listUserModule(query) {
  return request({
    url: '/system/user/module/list',
    method: 'get',
    params: query
  })
}

// 查询模块用户详细
export function getUserModule(id) {
  return request({
    url: '/system/user/module/' + id,
    method: 'get'
  })
}

// 新增模块用户
export function addUserModule(data) {
  return request({
    url: '/system/user/module',
    method: 'post',
    data: data
  })
}

// 新增模块用户
export function addGrantUserModule(data) {
  return request({
    url: '/system/user/module/addGrant',
    method: 'post',
    data: data
  })
}

// 修改模块用户
export function updateUserModule(data) {
  return request({
    url: '/system/user/module',
    method: 'put',
    data: data
  })
}

// 删除模块用户
export function delUserModule(id) {
  return request({
    url: '/system/user/module/' + id,
    method: 'delete'
  })
}

// 导出平台用户
export function exportUserModule(query) {
  return request({
    url: '/system/user/module/export',
    method: 'get',
    params: query
  })
}

// 下载用户导入模板
export function downloadTemplate() {
  return request({
    url: '/system/user/module/exportTemplate',
    method: 'get'
  })
}


// 模块用户登录
export function appLogin(data) {
  return request({
    url: '/app/login',
    method: 'post',
    data: data
  })
}

// 查询模块用户列表
export function getUserModules(token) {
  return request({
    url: '/app/user/modules/' + token,
    method: 'get'
  })
}
