import request from '@/utils/request'

// 查询导入配置列表
export function listConfig(query) {
  return request({
    url: '/system/import/config/list',
    method: 'get',
    params: query
  })
}

// 查询导入配置详细
export function getConfig(id) {
  return request({
    url: '/system/import/config/' + id,
    method: 'get'
  })
}

// 新增导入配置
export function addConfig(data) {
  return request({
    url: '/system/import/config',
    method: 'post',
    data: data
  })
}

// 修改导入配置
export function updateConfig(data) {
  return request({
    url: '/system/import/config',
    method: 'put',
    data: data
  })
}

// 删除导入配置
export function delConfig(id) {
  return request({
    url: '/system/import/config/' + id,
    method: 'delete'
  })
}

// 导出导入配置
export function exportConfig(query) {
  return request({
    url: '/system/import/config/export',
    method: 'get',
    params: query
  })
}
