import request from '@/utils/request'

// 查询导入配置明细列表
export function listDetails(query) {
  return request({
    url: '/system/import/config/details/list',
    method: 'get',
    params: query
  })
}

// 查询导入配置明细详细
export function getDetails(id) {
  return request({
    url: '/system/import/config/details/' + id,
    method: 'get'
  })
}

// 新增导入配置明细
export function addDetails(data) {
  return request({
    url: '/system/import/config/details',
    method: 'post',
    data: data
  })
}

// 修改导入配置明细
export function updateDetails(data) {
  return request({
    url: '/system/import/config/details',
    method: 'put',
    data: data
  })
}

// 删除导入配置明细
export function delDetails(id) {
  return request({
    url: '/system/import/config/details/' + id,
    method: 'delete'
  })
}

// 导出导入配置明细
export function exportDetails(query) {
  return request({
    url: '/system/import/config/details/export',
    method: 'get',
    params: query
  })
}

// 下载用户导入模板
export function downloadTemplate() {
  return request({
    url: '/system/user/module/exportTemplate',
    method: 'get'
  })
}
