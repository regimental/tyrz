package com.ruoyi.system.mapper;

import java.util.List;

import com.ruoyi.system.domain.BizImportConfig;

/**
 * 导入配置Mapper接口
 *
 * @author ruoyi
 * @date 2021-01-15
 */
public interface BizImportConfigMapper {
    /**
     * 查询导入配置
     *
     * @param id 导入配置ID
     * @return 导入配置
     */
    BizImportConfig selectBizImportConfigById(String id);

    /**
     * 查询导入配置列表
     *
     * @param bizImportConfig 导入配置
     * @return 导入配置集合
     */
    List<BizImportConfig> selectBizImportConfigList(BizImportConfig bizImportConfig);

    /**
     * 新增导入配置
     *
     * @param bizImportConfig 导入配置
     * @return 结果
     */
    int insertBizImportConfig(BizImportConfig bizImportConfig);

    /**
     * 修改导入配置
     *
     * @param bizImportConfig 导入配置
     * @return 结果
     */
    int updateBizImportConfig(BizImportConfig bizImportConfig);

    /**
     * 删除导入配置
     *
     * @param id 导入配置ID
     * @return 结果
     */
    int deleteBizImportConfigById(String id);

    /**
     * 批量删除导入配置
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deleteBizImportConfigByIds(String[] ids);
}
