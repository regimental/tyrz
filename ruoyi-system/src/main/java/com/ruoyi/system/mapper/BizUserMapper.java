package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.BizUser;

/**
 * 平台用户Mapper接口
 * 
 * @author ruoyi
 * @date 2020-11-12
 */
public interface BizUserMapper 
{
    /**
     * 查询平台用户
     * 
     * @param userId 平台用户ID
     * @return 平台用户
     */
    public BizUser selectBizUserById(String userId);

    /**
     * 查询平台用户列表
     * 
     * @param bizUser 平台用户
     * @return 平台用户集合
     */
    public List<BizUser> selectBizUserList(BizUser bizUser);

    /**
     * 新增平台用户
     * 
     * @param bizUser 平台用户
     * @return 结果
     */
    public int insertBizUser(BizUser bizUser);

    /**
     * 修改平台用户
     * 
     * @param bizUser 平台用户
     * @return 结果
     */
    public int updateBizUser(BizUser bizUser);

    /**
     * 删除平台用户
     * 
     * @param userId 平台用户ID
     * @return 结果
     */
    public int deleteBizUserById(String userId);

    /**
     * 批量删除平台用户
     * 
     * @param userIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteBizUserByIds(String[] userIds);
}
