package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.BizUserModule;

/**
 * 模块用户Mapper接口
 * 
 * @author ruoyi
 * @date 2020-11-12
 */
public interface BizUserModuleMapper 
{
    /**
     * 查询模块用户
     * 
     * @param id 模块用户ID
     * @return 模块用户
     */
    public BizUserModule selectBizUserModuleById(String id);

    /**
     * 查询模块用户列表
     * 
     * @param bizUserModule 模块用户
     * @return 模块用户集合
     */
    public List<BizUserModule> selectBizUserModuleList(BizUserModule bizUserModule);

    /**
     * 新增模块用户
     * 
     * @param bizUserModule 模块用户
     * @return 结果
     */
    public int insertBizUserModule(BizUserModule bizUserModule);

    /**
     * 修改模块用户
     * 
     * @param bizUserModule 模块用户
     * @return 结果
     */
    public int updateBizUserModule(BizUserModule bizUserModule);

    /**
     * 删除模块用户
     * 
     * @param id 模块用户ID
     * @return 结果
     */
    public int deleteBizUserModuleById(String id);

    /**
     * 批量删除模块用户
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBizUserModuleByIds(String[] ids);
}
