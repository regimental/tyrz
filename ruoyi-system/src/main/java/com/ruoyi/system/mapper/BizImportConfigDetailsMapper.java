package com.ruoyi.system.mapper;

import java.util.List;

import com.ruoyi.system.domain.BizImportConfigDetails;

/**
 * 【请填写功能名称】Mapper接口
 *
 * @author ruoyi
 * @date 2021-01-15
 */
public interface BizImportConfigDetailsMapper {
    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    BizImportConfigDetails selectBizImportConfigDetailsById(String id);

    /**
     * 查询【请填写功能名称】列表
     *
     * @param bizImportConfigDetails 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    List<BizImportConfigDetails> selectBizImportConfigDetailsList(BizImportConfigDetails bizImportConfigDetails);

    /**
     * 通过父键查询导入配置明细列表
     *
     * @param parentId 父键
     * @return 导入配置明细集合
     */
    List<BizImportConfigDetails> getListByParentId(String parentId);

    /**
     * 新增【请填写功能名称】
     *
     * @param bizImportConfigDetails 【请填写功能名称】
     * @return 结果
     */
    int insertBizImportConfigDetails(BizImportConfigDetails bizImportConfigDetails);

    /**
     * 修改【请填写功能名称】
     *
     * @param bizImportConfigDetails 【请填写功能名称】
     * @return 结果
     */
    int updateBizImportConfigDetails(BizImportConfigDetails bizImportConfigDetails);

    /**
     * 删除【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    int deleteBizImportConfigDetailsById(String id);

    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deleteBizImportConfigDetailsByIds(String[] ids);
}
