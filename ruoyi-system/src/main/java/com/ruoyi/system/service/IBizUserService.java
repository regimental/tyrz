package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.system.domain.BizUser;

/**
 * 模块用户Service接口
 *
 * @author ruoyi
 * @date 2020-11-12
 */
public interface IBizUserService {
    /**
     * 查询模块用户
     *
     * @param userId 模块用户ID
     * @return 模块用户
     */
    public BizUser selectBizUserById(String userId);

    /**
     * 查询模块用户列表
     *
     * @param bizUser 模块用户
     * @return 模块用户集合
     */
    public List<BizUser> selectBizUserList(BizUser bizUser);

    /**
     * 新增模块用户
     *
     * @param bizUser 模块用户
     * @return 结果
     */
    public int insertBizUser(BizUser bizUser);

    /**
     * SSO新增模块用户
     *
     * @param bizUser 模块用户
     * @return 结果
     */
    public int addBizUser(BizUser bizUser);

    /**
     * 修改模块用户
     *
     * @param bizUser 模块用户
     * @return 结果
     */
    public int updateBizUser(BizUser bizUser);

    /**
     * SSO修改模块用户
     *
     * @param bizUser 模块用户
     * @return 结果
     */
    public int editBizUser(BizUser bizUser);

    /**
     * 批量删除模块用户
     *
     * @param userIds 需要删除的模块用户ID
     * @return 结果
     */
    public int deleteBizUserByIds(String[] userIds);

    /**
     * 删除模块用户信息
     *
     * @param userId 模块用户ID
     * @return 结果
     */
    public int deleteBizUserById(String userId);

    /**
     * 删除模块用户信息
     *
     * @param bizUser 模块用户
     * @return 结果
     */
    public int removeBizUserById(BizUser bizUser);
}
