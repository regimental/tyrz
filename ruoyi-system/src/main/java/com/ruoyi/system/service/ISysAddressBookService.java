package com.ruoyi.system.service;


import com.ruoyi.system.domain.SysAddressBook;
import java.util.List;

/**
 * 通讯录 服务层
 *
 * @author pmj
 */
public interface ISysAddressBookService {

    /**
     * 查询通讯录信息
     *
     * @param addressBookId 通讯录ID
     * @return 通讯录信息
     */
    public SysAddressBook getAddressBookById(Long addressBookId);

    /**
     * 查询通讯录列表
     *
     * @param addressBook 通讯录信息
     * @return 通讯录集合
     */
    public List<SysAddressBook> getAddressBookList(SysAddressBook addressBook);

    /**
     * 新增通讯录
     *
     * @param addressBook 通讯录信息
     * @return 结果
     */
    public int insertAddressBook(SysAddressBook addressBook);

    /**
     * 修改通讯录
     *
     * @param addressBook 通讯录信息
     * @return 结果
     */
    public int updateAddressBook(SysAddressBook addressBook);

    /**
     * 删除通讯录信息
     *
     * @param addressBookId 通讯录ID
     * @return 结果
     */
    public int deleteAddressBookById(Long addressBookId);

    /**
     * 批量删除通讯录信息
     *
     * @param addressBookIds 需要删除的通讯录ID
     * @return 结果
     */
    public int deleteAddressBookByIds(Long[] addressBookIds);
}
