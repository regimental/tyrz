package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.model.LoginBody;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.exception.user.CaptchaException;
import com.ruoyi.common.exception.user.CaptchaExpireException;
import com.ruoyi.common.exception.user.UserPasswordNotMatchException;
import com.ruoyi.common.utils.*;
import com.ruoyi.system.domain.BizModule;
import com.ruoyi.system.domain.BizUser;
import com.ruoyi.system.domain.UserModule;
import com.ruoyi.system.service.IBizModuleService;
import com.ruoyi.system.service.IBizUserService;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BizUserModuleMapper;
import com.ruoyi.system.domain.BizUserModule;
import com.ruoyi.system.service.IBizUserModuleService;

import javax.security.auth.login.AccountNotFoundException;

/**
 * 模块用户Service业务层处理
 *
 * @author ruoyi
 * @date 2020-11-12
 */
@Service
public class BizUserModuleServiceImpl implements IBizUserModuleService {
    @Autowired
    private BizUserModuleMapper bizUserModuleMapper;
    @Autowired
    private IBizUserService bizUserService;
    @Autowired
    private IBizModuleService bizModuleService;
    @Autowired
    private RedisCache redisCache;

    /**
     * 查询模块用户
     *
     * @param id 模块用户ID
     * @return 模块用户
     */
    @Override
    public BizUserModule selectBizUserModuleById(String id) throws Exception {
        BizUserModule userModule = bizUserModuleMapper.selectBizUserModuleById(id);
        String password = AESUtils.decrypt(userModule.getAppUserPassword());
        userModule.setAppUserPassword(RSAUtils.encrypt(password));
        userModule.setUserPassword(RSAUtils.encrypt(userModule.getUserPassword()));
        return userModule;
    }

    /**
     * 查询模块用户列表
     *
     * @param bizUserModule 模块用户
     * @return 模块用户
     */
    @Override
    public List<BizUserModule> selectBizUserModuleList(BizUserModule bizUserModule) {
        return bizUserModuleMapper.selectBizUserModuleList(bizUserModule);
    }

    /**
     * SSO新增模块用户
     *
     * @param bizUserModule 模块用户
     * @return 结果
     */
    @Override
    public int addBizUserModule(BizUserModule bizUserModule) throws Exception {
        BizUser user = new BizUser();
        user.setUserId(UUID.randomUUID().toString());
        user.setUserDisplayname(bizUserModule.getAppUserDisplayname());
        String moduleShortNameEn = bizModuleService.selectBizModuleById(bizUserModule.getModuleId()).getModuleShortNameEn();
        if (!bizUserModule.getAppUserLogonname().startsWith(moduleShortNameEn + "_")) {
            user.setUserLogonname(moduleShortNameEn + "_" + bizUserModule.getAppUserLogonname());
        }
        List<BizUser> userList = bizUserService
                .selectBizUserList(new BizUser())
                .stream()
                .filter(t -> t.getUserLogonname().equals(user.getUserLogonname()))
                .collect(Collectors.toList());
        if (userList.size() > 0) {
            return -1;
        } else {
            String password;
            if (!StringUtils.isEmpty(bizUserModule.getAppUserPassword())) {
                password = RSAUtils.decrypt(bizUserModule.getAppUserPassword());
            } else {
                password = "123456";
            }
            user.setUserPassword(AESUtils.encrypt(password));
            user.setIsDel((long) 0);
            user.setCreateBy("SSO");
            user.setCreateTime(DateUtils.getNowDate());
            bizUserService.addBizUser(user);

            bizUserModule.setId(UUID.randomUUID().toString());
            bizUserModule.setIsDel((long) 0);
            bizUserModule.setUserId(user.getUserId());
            bizUserModule.setUserPassword(bizUserModule.getUserPassword());//RSAUtils.decrypt(bizUserModule.getUserPassword())
            bizUserModule.setCreateBy("SSO");
            bizUserModule.setCreateTime(DateUtils.getNowDate());
            return bizUserModuleMapper.insertBizUserModule(bizUserModule);
        }
    }

    /**
     * 新增模块用户
     *
     * @param bizUserModule 模块用户
     * @return 结果
     */
    @Override
    public int insertBizUserModule(BizUserModule bizUserModule) throws Exception {
        BizUser user = new BizUser();
        user.setUserId(UUID.randomUUID().toString());
        user.setUserDisplayname(bizUserModule.getAppUserDisplayname());
        String moduleShortNameEn = bizModuleService.selectBizModuleById(bizUserModule.getModuleId()).getModuleShortNameEn();
        if (!bizUserModule.getAppUserLogonname().startsWith(moduleShortNameEn + "_")) {
            user.setUserLogonname(moduleShortNameEn + "_" + bizUserModule.getAppUserLogonname());
        }
        List<BizUser> userList = bizUserService
                .selectBizUserList(new BizUser())
                .stream()
                .filter(t -> t.getUserLogonname().equals(user.getUserLogonname()))
                .collect(Collectors.toList());
        if (userList.size() > 0) {
            return -1;
        } else {
            String password;
            if (!StringUtils.isEmpty(bizUserModule.getAppUserPassword())) {
                password = RSAUtils.decrypt(bizUserModule.getAppUserPassword());
            } else {
                password = "123456";
            }
            user.setUserPassword(AESUtils.encrypt(password));
            user.setIsDel((long) 0);
            user.setCreateBy(SecurityUtils.getUsername());
            user.setCreateTime(DateUtils.getNowDate());
            bizUserService.insertBizUser(user);

            bizUserModule.setId(UUID.randomUUID().toString());
            bizUserModule.setIsDel((long) 0);
            bizUserModule.setUserId(user.getUserId());
            bizUserModule.setUserPassword(RSAUtils.decrypt(bizUserModule.getUserPassword()));
            bizUserModule.setCreateBy(SecurityUtils.getUsername());
            bizUserModule.setCreateTime(DateUtils.getNowDate());
            return bizUserModuleMapper.insertBizUserModule(bizUserModule);
        }
    }

    /**
     * 新增模块用户
     *
     * @param bizUserModule 新增用户授权
     * @return 结果
     */
    public int addGrant(BizUserModule bizUserModule) throws Exception {
        List<BizUserModule> userModuleList = bizUserModuleMapper
                .selectBizUserModuleList(new BizUserModule())
                .stream()
                .filter(t -> t.getUserId().equals(bizUserModule.getUserId()) && t.getModuleId().equals(bizUserModule.getModuleId()))
                .collect(Collectors.toList());
        if (userModuleList.size() > 0) {
            return -1;
        }
        bizUserModule.setId(UUID.randomUUID().toString());
        bizUserModule.setIsDel((long) 0);
        bizUserModule.setUserPassword(RSAUtils.decrypt(bizUserModule.getUserPassword()));
        bizUserModule.setCreateBy(SecurityUtils.getUsername());
        bizUserModule.setCreateTime(DateUtils.getNowDate());
        return bizUserModuleMapper.insertBizUserModule(bizUserModule);
    }

    /**
     * 修改模块用户
     *
     * @param bizUserModule 模块用户
     * @return 结果
     */
    @Override
    public int updateBizUserModule(BizUserModule bizUserModule) throws Exception {
        BizUser user = bizUserService.selectBizUserById(bizUserModule.getUserId());
        user.setUserDisplayname(bizUserModule.getAppUserDisplayname());
        if (!user.getUserLogonname().equals(bizUserModule.getAppUserLogonname())) {
            String moduleShortNameEn = bizModuleService.selectBizModuleById(bizUserModule.getModuleId()).getModuleShortNameEn();
            if (!bizUserModule.getAppUserLogonname().startsWith(moduleShortNameEn + "_")) {
                user.setUserLogonname(moduleShortNameEn + "_" + bizUserModule.getAppUserLogonname());
            } else {
                user.setUserLogonname(bizUserModule.getAppUserLogonname());
            }
        }
        List<BizUser> userList = bizUserService
                .selectBizUserList(new BizUser())
                .stream()
                .filter(t -> !t.getUserId().equals(user.getUserId()) && t.getUserLogonname().equals(user.getUserLogonname()))
                .collect(Collectors.toList());
        if (userList.size() > 0) {
            return -1;
        } else {
            String password = RSAUtils.decrypt(bizUserModule.getAppUserPassword());
            password = AESUtils.encrypt(password);
            if (!user.getUserPassword().equals(password)) {
                user.setUserPassword(password);
            }
            user.setUpdateBy(SecurityUtils.getUsername());
            user.setUpdateTime(DateUtils.getNowDate());
            bizUserService.updateBizUser(user);

            bizUserModule.setUserPassword(RSAUtils.decrypt(bizUserModule.getUserPassword()));
            bizUserModule.setUpdateBy(SecurityUtils.getUsername());
            bizUserModule.setUpdateTime(DateUtils.getNowDate());
            return bizUserModuleMapper.updateBizUserModule(bizUserModule);
        }
    }

    /**
     * 批量删除模块用户
     *
     * @param ids 需要删除的模块用户ID
     * @return 结果
     */
    @Override
    public int deleteBizUserModuleByIds(String[] ids) {
        //return bizUserModuleMapper.deleteBizUserModuleByIds(ids);
        for (String id : ids) {
            this.remove(id);
        }
        return 1;
    }

    private int remove(String id) {
        BizUserModule bizUserModule = bizUserModuleMapper.selectBizUserModuleById(id);

        BizUserModule newUserModule = new BizUserModule();
        newUserModule.setUserId(bizUserModule.getUserId());
        List<BizUserModule> userModuleList = bizUserModuleMapper.selectBizUserModuleList(newUserModule);
        if (userModuleList.size() == 1) {
            bizUserService.deleteBizUserById(userModuleList.stream().findFirst().get().getUserId());
        }

        bizUserModule.setIsDel((long) 1);
        bizUserModule.setUpdateBy(SecurityUtils.getUsername());
        bizUserModule.setUpdateTime(DateUtils.getNowDate());
        return bizUserModuleMapper.updateBizUserModule(bizUserModule);
    }

    /**
     * 删除模块用户信息
     *
     * @param id 模块用户ID
     * @return 结果
     */
    @Override
    public int deleteBizUserModuleById(String id) {
        //return bizUserModuleMapper.deleteBizUserModuleById(id);
        return this.remove(id);
    }

    /**
     * 删除模块用户信息
     *
     * @param moduleId 模块用户ID
     * @return 结果
     */
    @Override
    public StringBuilder importData(List<BizUserModule> bizUserModules, String moduleId) {
        StringBuilder msg = new StringBuilder("");
        bizUserModules.forEach(item -> {
            BizUser user = new BizUser();
            user.setUserId(UUID.randomUUID().toString());
            user.setUserDisplayname(item.getAppUserDisplayname());
            String moduleShortNameEn = bizModuleService.selectBizModuleById(moduleId).getModuleShortNameEn();
            if (!item.getAppUserLogonname().startsWith(moduleShortNameEn + "_")) {
                user.setUserLogonname(moduleShortNameEn + "_" + item.getAppUserLogonname());
            }
            List<BizUser> userList = bizUserService
                    .selectBizUserList(new BizUser())
                    .stream()
                    .filter(t -> t.getUserLogonname().equals(user.getUserLogonname()))
                    .collect(Collectors.toList());
            if (userList.size() > 0) {
                msg.append(user.getUserLogonname() + "已存在，");
            } else {
                String password = item.getAppUserPassword();
                try {
                    user.setUserPassword(AESUtils.encrypt(StringUtils.isEmpty(password) ? "123456" : password));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                user.setIsDel((long) 0);
                user.setCreateBy(SecurityUtils.getUsername());
                user.setCreateTime(DateUtils.getNowDate());
                bizUserService.insertBizUser(user);

                BizUserModule bizUserModule = new BizUserModule();
                bizUserModule.setId(UUID.randomUUID().toString());
                bizUserModule.setUserId(user.getUserId());
                bizUserModule.setModuleId(moduleId);
                bizUserModule.setUserLogonname(item.getUserLogonname());
                bizUserModule.setUserPassword(item.getUserPassword());
                bizUserModule.setIsDel((long) 0);
                bizUserModule.setCreateBy(SecurityUtils.getUsername());
                bizUserModule.setCreateTime(DateUtils.getNowDate());
                bizUserModuleMapper.insertBizUserModule(bizUserModule);
            }
        });
        return msg;
    }

    /**
     * 修改模块用户
     *
     * @param bizUserModule 模块用户
     * @return 结果
     */
    @Override
    public int edit(BizUserModule bizUserModule) throws Exception {
        String moduleShortNameEn = bizModuleService.selectBizModuleById(bizUserModule.getModuleId()).getModuleShortNameEn();
        String logonName = moduleShortNameEn + "_" + bizUserModule.getAppUserLogonname();
        List<BizUser> userList = bizUserService
                .selectBizUserList(new BizUser())
                .stream()
                .filter(t -> t.getUserLogonname().equals(logonName))
                .collect(Collectors.toList());
        if (userList.size() > 0) {
            BizUser user = userList.stream().findFirst().get();
            user.setUserDisplayname(bizUserModule.getAppUserDisplayname());
            String password;
            if (!StringUtils.isEmpty(bizUserModule.getAppUserPassword())) {
                password = RSAUtils.decrypt(bizUserModule.getAppUserPassword());
            } else {
                password = "123456";
            }
            user.setUserPassword(AESUtils.encrypt(password));
            user.setUpdateBy("SSO");
            user.setUpdateTime(DateUtils.getNowDate());
            bizUserService.editBizUser(user);

            bizUserModule.setUserPassword(bizUserModule.getUserPassword());//RSAUtils.decrypt(bizUserModule.getUserPassword())
            bizUserModule.setUpdateBy("SSO");
            bizUserModule.setUpdateTime(DateUtils.getNowDate());
            return bizUserModuleMapper.updateBizUserModule(bizUserModule);
        } else {
            return -1;
        }
    }

    /**
     * 删除模块用户信息
     *
     * @param logonNames 模块用户logonNames
     * @param moduleId   moduleId
     * @return 结果
     */
    @Override
    public int deleteByLogonName(String[] logonNames, String moduleId) {
        int rows = 0;
        String moduleShortNameEn = bizModuleService.selectBizModuleById(moduleId).getModuleShortNameEn();
        for (String item : logonNames) {
            String logonName = moduleShortNameEn + "_" + item;
            List<BizUser> userList = bizUserService
                    .selectBizUserList(new BizUser())
                    .stream()
                    .filter(t -> t.getUserLogonname().equals(logonName))
                    .collect(Collectors.toList());
            if (userList.size() > 0) {
                BizUser user = userList.stream().findFirst().get();
                user.setIsDel((long) 1);
                user.setUpdateBy("SSO");
                user.setUpdateTime(DateUtils.getNowDate());
                bizUserService.removeBizUserById(user);

                BizUserModule bizUserModule = bizUserModuleMapper.selectBizUserModuleList(new BizUserModule())
                        .stream()
                        .filter(t -> t.getUserId().equals(user.getUserId()) && t.getModuleId().equals(moduleId))
                        .collect(Collectors.toList())
                        .get(0);

                bizUserModule.setIsDel((long) 1);
                bizUserModule.setUpdateBy("SSO");
                bizUserModule.setUpdateTime(DateUtils.getNowDate());
                bizUserModuleMapper.updateBizUserModule(bizUserModule);
                rows++;
            }
        }
        return rows;
    }

    /**
     * 模块用户登录
     *
     * @param loginBody 登录信息
     * @return 结果
     */
    @Override
    public String login(LoginBody loginBody) throws Exception {
        String token = new String("");
        String verifyKey = Constants.CAPTCHA_CODE_KEY + loginBody.getUuid();
        String captcha = redisCache.getCacheObject(verifyKey);
        redisCache.deleteObject(verifyKey);
        if (captcha == null) {
            throw new CaptchaExpireException();
        }
        if (!loginBody.getCode().equalsIgnoreCase(captcha)) {
            throw new CaptchaException();
        }
        BizUser query = new BizUser();
        query.setUserLogonname(loginBody.getUsername());
        List<BizUser> userList = bizUserService.selectBizUserList(query)
                .stream().collect(Collectors.toList());
        if (userList.size() > 0) {
            BizUser user = userList.get(0);
            String userPassword = user.getUserPassword();
            String encryptPassword = RSAUtils.decrypt(loginBody.getPassword());
            if (userPassword.equals(AESUtils.encrypt(encryptPassword))) {
                token = Constants.APP_USER_KEY + UUID.randomUUID();
                redisCache.setCacheObject(token, user, 20, TimeUnit.MINUTES);
            } else {
                throw new UserPasswordNotMatchException();
            }
        } else {
            throw new AccountNotFoundException("用户不存在");
        }

        return token;
    }

    /**
     * 获取用户模块
     *
     * @param token 用户token
     * @return 结果
     */
    @Override
    public List<UserModule> getUserModuleList(String token) {
        List<UserModule> userModuleList = new ArrayList<>();

        BizUser user = redisCache.getCacheObject(token);

        List<BizModule> modules = bizModuleService.selectBizModuleList(new BizModule());

        List<BizUserModule> userModules = bizUserModuleMapper.
                selectBizUserModuleList(new BizUserModule()).
                stream().
                filter(t -> t.getUserId().equals(user.getUserId())).
                collect(Collectors.toList());

        modules.forEach(item -> {
            UserModule userModule = new UserModule();
            userModule.setModuleId(item.getModuleId());
            userModule.setModuleName(item.getModuleName());
            userModule.setModuleShortName(item.getModuleShortName());
            userModule.setModuleShortNameEn(item.getModuleShortNameEn());
            userModule.setModuleProtocol(item.getModuleProtocol());
            userModule.setModuleAddress(item.getModuleAddress());
            userModule.setModulePort(item.getModulePort());
            userModule.setModuleLogoAddress(item.getModuleLogoAddress());
            userModule.setModuleSsoAddress(item.getModuleSsoAddress());
            userModule.setModuleTodoAddress(item.getModuleTodoAddress());
            userModule.setNo(item.getNo());
            userModule.setAppUserDisplayname(user.getUserDisplayname());

            List<BizUserModule> ums = userModules.
                    stream().
                    filter(t -> t.getModuleId().equals(item.getModuleId())).
                    collect(Collectors.toList());

            if (ums.size() > 0) {
                BizUserModule um = bizUserModuleMapper.selectBizUserModuleById(ums.get(0).getId());
                userModule.setUserLogonname(um.getUserLogonname());
                try {
                    userModule.setUserPassword(RSAUtils.encrypt(um.getUserPassword()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            userModuleList.add(userModule);
        });
        return userModuleList;
    }
}
