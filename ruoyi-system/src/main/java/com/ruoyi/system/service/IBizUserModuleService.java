package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.common.core.domain.model.LoginBody;
import com.ruoyi.system.domain.BizUserModule;
import com.ruoyi.system.domain.UserModule;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.security.auth.login.AccountNotFoundException;

/**
 * 模块用户Service接口
 *
 * @author ruoyi
 * @date 2020-11-12
 */
public interface IBizUserModuleService {
    /**
     * 查询模块用户
     *
     * @param id 模块用户ID
     * @return 模块用户
     */
    public BizUserModule selectBizUserModuleById(String id) throws Exception;

    /**
     * 查询模块用户列表
     *
     * @param bizUserModule 模块用户
     * @return 模块用户集合
     */
    public List<BizUserModule> selectBizUserModuleList(BizUserModule bizUserModule);

    /**
     * 新增模块用户
     *
     * @param bizUserModule 模块用户
     * @return 结果
     */
    public int insertBizUserModule(BizUserModule bizUserModule) throws Exception;

    /**
     * SSO新增模块用户
     *
     * @param bizUserModule 模块用户
     * @return 结果
     */
    public int addBizUserModule(BizUserModule bizUserModule) throws Exception;

    /**
     * 新增模块用户
     *
     * @param bizUserModule 新增用户授权
     * @return 结果
     */
    public int addGrant(BizUserModule bizUserModule) throws Exception;

    /**
     * 修改模块用户
     *
     * @param bizUserModule 模块用户
     * @return 结果
     */
    public int updateBizUserModule(BizUserModule bizUserModule) throws Exception;

    /**
     * 批量删除模块用户
     *
     * @param ids 需要删除的模块用户ID
     * @return 结果
     */
    public int deleteBizUserModuleByIds(String[] ids);

    /**
     * 删除模块用户信息
     *
     * @param id 模块用户ID
     * @return 结果
     */
    public int deleteBizUserModuleById(String id);

    /**
     * 删除模块用户信息
     *
     * @param moduleId 模块用户ID
     * @return 结果
     */
    public StringBuilder importData(List<BizUserModule> bizUserModules, String moduleId);

    /**
     * 修改模块用户
     *
     * @param bizUserModule 模块用户
     * @return 结果
     */
    public int edit(BizUserModule bizUserModule) throws Exception;

    /**
     * 删除模块用户信息
     *
     * @param logonNames 模块用户logonNames
     * @param moduleId   moduleId
     * @return 结果
     */
    public int deleteByLogonName(String[] logonNames, String moduleId);

    /**
     * 模块用户登录
     *
     * @param loginBody 登录信息
     * @return 结果
     */
    public String login(LoginBody loginBody) throws Exception;

    /**
     * 获取用户模块
     *
     * @param token 用户token
     * @return 结果
     */
    public List<UserModule> getUserModuleList(String token);
}
