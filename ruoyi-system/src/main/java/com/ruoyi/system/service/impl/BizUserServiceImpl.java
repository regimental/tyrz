package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.UUID;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BizUserMapper;
import com.ruoyi.system.domain.BizUser;
import com.ruoyi.system.service.IBizUserService;

/**
 * 模块用户Service业务层处理
 *
 * @author ruoyi
 * @date 2020-11-12
 */
@Service
public class BizUserServiceImpl implements IBizUserService {
    @Autowired
    private BizUserMapper bizUserMapper;

    /**
     * 查询模块用户
     *
     * @param userId 模块用户ID
     * @return 模块用户
     */
    @Override
    public BizUser selectBizUserById(String userId) {
        return bizUserMapper.selectBizUserById(userId);
    }

    /**
     * 查询模块用户列表
     *
     * @param bizUser 模块用户
     * @return 模块用户
     */
    @Override
    public List<BizUser> selectBizUserList(BizUser bizUser) {
        return bizUserMapper.selectBizUserList(bizUser);
    }

    /**
     * 新增模块用户
     *
     * @param bizUser 模块用户
     * @return 结果
     */
    @Override
    public int insertBizUser(BizUser bizUser) {
        bizUser.setUserId(UUID.randomUUID().toString());
        bizUser.setIsDel((long) 0);
        bizUser.setCreateTime(DateUtils.getNowDate());
        bizUser.setCreateBy(SecurityUtils.getUsername());
        return bizUserMapper.insertBizUser(bizUser);
    }

    /**
     * SSO新增模块用户
     *
     * @param bizUser 模块用户
     * @return 结果
     */
    @Override
    public int addBizUser(BizUser bizUser) {
        return bizUserMapper.insertBizUser(bizUser);
    }

    /**
     * 修改模块用户
     *
     * @param bizUser 模块用户
     * @return 结果
     */
    @Override
    public int updateBizUser(BizUser bizUser) {
        bizUser.setUpdateTime(DateUtils.getNowDate());
        bizUser.setUpdateBy(SecurityUtils.getUsername());
        return bizUserMapper.updateBizUser(bizUser);
    }

    /**
     * SSO修改模块用户
     *
     * @param bizUser 模块用户
     * @return 结果
     */
    @Override
    public int editBizUser(BizUser bizUser) {
        return bizUserMapper.updateBizUser(bizUser);
    }

    /**
     * 批量删除模块用户
     *
     * @param userIds 需要删除的模块用户ID
     * @return 结果
     */
    @Override
    public int deleteBizUserByIds(String[] userIds) {
        for (String userId : userIds) {
            this.remove(userId);
        }
        return 1;
        //return bizUserMapper.deleteBizUserByIds(userIds);
    }

    private int remove(String userId) {
        BizUser bizUser = bizUserMapper.selectBizUserById(userId);
        bizUser.setIsDel((long) 1);
        bizUser.setUpdateTime(DateUtils.getNowDate());
        bizUser.setUpdateBy(SecurityUtils.getUsername());
        return bizUserMapper.updateBizUser(bizUser);
    }

    /**
     * 删除模块用户信息
     *
     * @param userId 模块用户ID
     * @return 结果
     */
    @Override
    public int deleteBizUserById(String userId) {
        return this.remove(userId);
    }

    /**
     * 删除模块用户信息
     *
     * @param bizUser 模块用户
     * @return 结果
     */
    @Override
    public int removeBizUserById(BizUser bizUser) {
        return bizUserMapper.updateBizUser(bizUser);
    }
}
