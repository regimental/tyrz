package com.ruoyi.system.service;

import com.ruoyi.system.domain.BizImportConfigDetails;

import java.util.List;

/**
 * 导入配置明细Service接口
 *
 * @author ruoyi
 * @date 2021-01-15
 */
public interface IBizImportConfigDetailsService {
    /**
     * 查询导入配置明细
     *
     * @param id 导入配置明细ID
     * @return 导入配置明细
     */
    BizImportConfigDetails selectBizImportConfigDetailsById(String id);

    /**
     * 查询导入配置明细列表
     *
     * @param bizImportConfigDetails 导入配置明细
     * @return 导入配置明细集合
     */
    List<BizImportConfigDetails> selectBizImportConfigDetailsList(BizImportConfigDetails bizImportConfigDetails);

    /**
     * 通过父键查询导入配置明细列表
     *
     * @param parentId 父键
     * @return 导入配置明细集合
     */
    List<BizImportConfigDetails> getListByParentId(String parentId);

    /**
     * 新增导入配置明细
     *
     * @param bizImportConfigDetails 导入配置明细
     * @return 结果
     */
    int insertBizImportConfigDetails(BizImportConfigDetails bizImportConfigDetails);

    /**
     * 修改导入配置明细
     *
     * @param bizImportConfigDetails 导入配置明细
     * @return 结果
     */
    int updateBizImportConfigDetails(BizImportConfigDetails bizImportConfigDetails);

    /**
     * 批量删除导入配置明细
     *
     * @param ids 需要删除的导入配置明细ID
     * @return 结果
     */
    int deleteBizImportConfigDetailsByIds(String[] ids);

    /**
     * 删除导入配置明细信息
     *
     * @param id 导入配置明细ID
     * @return 结果
     */
    int deleteBizImportConfigDetailsById(String id);
}
