package com.ruoyi.system.service.base;

import java.util.List;

public interface IBaseService<T> {

    T getById(Object id);

    List<T> getList(T t);

    int insert(T t);

    int update(T t);

    int delete(Object id);

    int delete(Object[] ids);
}
