package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.SysAddressBook;
import com.ruoyi.system.mapper.SysAddressBookMapper;
import com.ruoyi.system.service.ISysAddressBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 通讯录 服务层实现
 *
 * @author pmj
 */
@Service
public class SysAddressBookImpl implements ISysAddressBookService {

    @Autowired
    private SysAddressBookMapper addressBookMapper;

    /**
     * 查询通讯录信息
     *
     * @param addressBookId 通讯录ID
     * @return 通讯录信息
     */
    public SysAddressBook getAddressBookById(Long addressBookId){
        return  addressBookMapper.getAddressBookById(addressBookId);
    }

    /**
     * 查询通讯录列表
     *
     * @param addressBook 通讯录信息
     * @return 通讯录集合
     */
    public List<SysAddressBook> getAddressBookList(SysAddressBook addressBook){
        return addressBookMapper.getAddressBookList(addressBook);
    }

    /**
     * 新增通讯录
     *
     * @param addressBook 通讯录信息
     * @return 结果
     */
    public int insertAddressBook(SysAddressBook addressBook){
        return addressBookMapper.insertAddressBook(addressBook);
    }

    /**
     * 修改通讯录
     *
     * @param addressBook 通讯录信息
     * @return 结果
     */
    public int updateAddressBook(SysAddressBook addressBook){
        return addressBookMapper.updateAddressBook(addressBook);
    }

    /**
     * 删除通讯录信息
     *
     * @param addressBookId 通讯录ID
     * @return 结果
     */
    public int deleteAddressBookById(Long addressBookId){
        return addressBookMapper.deleteAddressBookById(addressBookId);
    }

    /**
     * 批量删除通讯录信息
     *
     * @param addressBookIds 需要删除的通讯录ID
     * @return 结果
     */
    public int deleteAddressBookByIds(Long[] addressBookIds){
        return addressBookMapper.deleteAddressBookByIds(addressBookIds);
    }
}
