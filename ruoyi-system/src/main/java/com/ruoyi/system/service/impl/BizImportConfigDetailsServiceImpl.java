package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.UUID;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BizImportConfigDetailsMapper;
import com.ruoyi.system.domain.BizImportConfigDetails;
import com.ruoyi.system.service.IBizImportConfigDetailsService;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author ruoyi
 * @date 2021-01-15
 */
@Service
public class BizImportConfigDetailsServiceImpl implements IBizImportConfigDetailsService {
    @Autowired
    private BizImportConfigDetailsMapper bizImportConfigDetailsMapper;

    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public BizImportConfigDetails selectBizImportConfigDetailsById(String id) {
        return bizImportConfigDetailsMapper.selectBizImportConfigDetailsById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     *
     * @param bizImportConfigDetails 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<BizImportConfigDetails> selectBizImportConfigDetailsList(BizImportConfigDetails bizImportConfigDetails) {
        return bizImportConfigDetailsMapper.selectBizImportConfigDetailsList(bizImportConfigDetails);
    }

    /**
     * 通过父键查询导入配置明细列表
     *
     * @param parentId 父键
     * @return 导入配置明细集合
     */
    @Override
    public List<BizImportConfigDetails> getListByParentId(String parentId) {
        return bizImportConfigDetailsMapper.getListByParentId(parentId);
    }

    /**
     * 新增【请填写功能名称】
     *
     * @param bizImportConfigDetails 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertBizImportConfigDetails(BizImportConfigDetails bizImportConfigDetails) {
        bizImportConfigDetails.setId(UUID.randomUUID().toString());
        bizImportConfigDetails.setIsDel((long) 0);
        bizImportConfigDetails.setCreateBy(SecurityUtils.getUsername());
        bizImportConfigDetails.setCreateTime(DateUtils.getNowDate());

        if (bizImportConfigDetails.getFloatLimit() <= 0) {
            bizImportConfigDetails.setFloatLimit(null);
        }

        return bizImportConfigDetailsMapper.insertBizImportConfigDetails(bizImportConfigDetails);
    }

    /**
     * 修改【请填写功能名称】
     *
     * @param bizImportConfigDetails 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateBizImportConfigDetails(BizImportConfigDetails bizImportConfigDetails) {
        bizImportConfigDetails.setUpdateBy(SecurityUtils.getUsername());
        bizImportConfigDetails.setUpdateTime(DateUtils.getNowDate());
        return bizImportConfigDetailsMapper.updateBizImportConfigDetails(bizImportConfigDetails);
    }

    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteBizImportConfigDetailsByIds(String[] ids) {
        return bizImportConfigDetailsMapper.deleteBizImportConfigDetailsByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     *
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteBizImportConfigDetailsById(String id) {
        return bizImportConfigDetailsMapper.deleteBizImportConfigDetailsById(id);
    }
}
