package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.UUID;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BizModuleMapper;
import com.ruoyi.system.domain.BizModule;
import com.ruoyi.system.service.IBizModuleService;

/**
 * 模块管理Service业务层处理
 *
 * @author ruoyi
 * @date 2020-11-12
 */
@Service
public class BizModuleServiceImpl implements IBizModuleService {
    @Autowired
    private BizModuleMapper bizModuleMapper;

    /**
     * 查询模块
     *
     * @param moduleId 模块ID
     * @return 模块
     */
    @Override
    public BizModule selectBizModuleById(String moduleId) {
        return bizModuleMapper.selectBizModuleById(moduleId);
    }

    /**
     * 查询模块列表
     *
     * @param bizModule 模块
     * @return 模块集合
     */
    @Override
    public List<BizModule> selectBizModuleList(BizModule bizModule) {
        return bizModuleMapper.selectBizModuleList(bizModule);
    }

    /**
     * 查询授权模块列表
     *
     * @param userId 模块
     * @return 模块集合
     */
    @Override
    public List<BizModule> selectBizModuleGrantList(String userId) {
        return bizModuleMapper.selectBizModuleGrantList(userId);
    }

    /**
     * 新增模块
     *
     * @param bizModule 模块
     * @return 结果
     */
    @Override
    public int insertBizModule(BizModule bizModule) {
        bizModule.setModuleId(UUID.randomUUID().toString());
        bizModule.setIsDel((long) 0);
        bizModule.setCreateBy(SecurityUtils.getUsername());
        bizModule.setCreateTime(DateUtils.getNowDate());
        return bizModuleMapper.insertBizModule(bizModule);
    }

    /**
     * 修改模块
     *
     * @param bizModule 模块
     * @return 结果
     */
    @Override
    public int updateBizModule(BizModule bizModule) {
        bizModule.setUpdateTime(DateUtils.getNowDate());
        bizModule.setUpdateBy(SecurityUtils.getUsername());
        return bizModuleMapper.updateBizModule(bizModule);
    }

    /**
     * 批量删除模块
     *
     * @param moduleIds 需要删除的模块ID
     * @return 结果
     */
    @Override
    public int deleteBizModuleByIds(String[] moduleIds) {
        for (String moduleId : moduleIds) {
            this.remove(moduleId);
        }
        //return bizModuleMapper.deleteBizModuleByIds(moduleIds);
        return 1;
    }

    private int remove(String moduleId) {
        BizModule module = bizModuleMapper.selectBizModuleById(moduleId);
        module.setIsDel((long) 1);
        module.setUpdateBy(SecurityUtils.getUsername());
        module.setUpdateTime(DateUtils.getNowDate());
        return bizModuleMapper.updateBizModule(module);
    }

    /**
     * 删除模块信息
     *
     * @param moduleId 模块ID
     * @return 结果
     */
    @Override
    public int deleteBizModuleById(String moduleId) {
        return this.remove(moduleId);
    }
}
