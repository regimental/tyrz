package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.BizModule;

/**
 * 模块管理Service接口
 * 
 * @author ruoyi
 * @date 2020-11-12
 */
public interface IBizModuleService 
{
    /**
     * 查询模块
     * 
     * @param moduleId 模块ID
     * @return 模块管理
     */
    public BizModule selectBizModuleById(String moduleId);

    /**
     * 查询模块列表
     * 
     * @param bizModule 模块
     * @return 模块集合
     */
    public List<BizModule> selectBizModuleList(BizModule bizModule);

    /**
     * 查询授权模块列表
     *
     * @param userId 模块
     * @return 模块集合
     */
    public List<BizModule> selectBizModuleGrantList(String userId);

    /**
     * 新增模块
     * 
     * @param bizModule 模块
     * @return 结果
     */
    public int insertBizModule(BizModule bizModule);

    /**
     * 修改模块
     * 
     * @param bizModule 模块
     * @return 结果
     */
    public int updateBizModule(BizModule bizModule);

    /**
     * 批量删除模块
     * 
     * @param moduleIds 需要删除的模块ID
     * @return 结果
     */
    public int deleteBizModuleByIds(String[] moduleIds);

    /**
     * 删除模块信息
     * 
     * @param moduleId 模块ID
     * @return 结果
     */
    public int deleteBizModuleById(String moduleId);
}
