package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.UUID;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BizImportConfigMapper;
import com.ruoyi.system.domain.BizImportConfig;
import com.ruoyi.system.service.IBizImportConfigService;

/**
 * 导入配置Service业务层处理
 *
 * @author ruoyi
 * @date 2021-01-15
 */
@Service
public class BizImportConfigServiceImpl implements IBizImportConfigService {
    @Autowired
    private BizImportConfigMapper bizImportConfigMapper;

    /**
     * 查询导入配置
     *
     * @param id 导入配置ID
     * @return 导入配置
     */
    @Override
    public BizImportConfig selectBizImportConfigById(String id) {
        return bizImportConfigMapper.selectBizImportConfigById(id);
    }

    /**
     * 查询导入配置列表
     *
     * @param bizImportConfig 导入配置
     * @return 导入配置
     */
    @Override
    public List<BizImportConfig> selectBizImportConfigList(BizImportConfig bizImportConfig) {
        return bizImportConfigMapper.selectBizImportConfigList(bizImportConfig);
    }

    /**
     * 新增导入配置
     *
     * @param bizImportConfig 导入配置
     * @return 结果
     */
    @Override
    public int insertBizImportConfig(BizImportConfig bizImportConfig) {
        bizImportConfig.setId(UUID.randomUUID().toString());
        bizImportConfig.setIsDel((long) 0);
        bizImportConfig.setCreateBy(SecurityUtils.getUsername());
        bizImportConfig.setCreateTime(DateUtils.getNowDate());
        return bizImportConfigMapper.insertBizImportConfig(bizImportConfig);
    }

    /**
     * 修改导入配置
     *
     * @param bizImportConfig 导入配置
     * @return 结果
     */
    @Override
    public int updateBizImportConfig(BizImportConfig bizImportConfig) {
        bizImportConfig.setUpdateBy(SecurityUtils.getUsername());
        bizImportConfig.setUpdateTime(DateUtils.getNowDate());
        return bizImportConfigMapper.updateBizImportConfig(bizImportConfig);
    }

    /**
     * 批量删除导入配置
     *
     * @param ids 需要删除的导入配置ID
     * @return 结果
     */
    @Override
    public int deleteBizImportConfigByIds(String[] ids) {
        int rows = 0;
        for (String id : ids) {
            BizImportConfig importConfig = bizImportConfigMapper.selectBizImportConfigById(id);
            importConfig.setIsDel((long) 1);
            importConfig.setUpdateBy(SecurityUtils.getUsername());
            importConfig.setUpdateTime(DateUtils.getNowDate());
            bizImportConfigMapper.updateBizImportConfig(importConfig);
            rows++;
        }
        return rows;
        //return bizImportConfigMapper.deleteBizImportConfigByIds(ids);
    }

    /**
     * 删除导入配置信息
     *
     * @param id 导入配置ID
     * @return 结果
     */
    @Override
    public int deleteBizImportConfigById(String id) {
        return bizImportConfigMapper.deleteBizImportConfigById(id);
    }
}
