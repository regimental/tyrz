package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 模块管理对象 biz_module
 *
 * @author ruoyi
 * @date 2020-11-12
 */
public class BizModule extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 模块编号
     */
    private String moduleId;

    /**
     * 模块名称
     */
    @Excel(name = "模块名称")
    private String moduleName;

    /**
     * 模块简称
     */
    @Excel(name = "模块简称")
    private String moduleShortName;

    /**
     * 模块英文简称
     */
    @Excel(name = "模块英文简称")
    private String moduleShortNameEn;

    /**
     * 模块英文简称
     */
    @Excel(name = "模块HTTP协议")
    private String moduleProtocol;

    /**
     * 模块地址：ip或者域名
     */
    @Excel(name = "模块地址")
    private String moduleAddress;

    /**
     * 模块端口
     */
    @Excel(name = "模块端口")
    private Long modulePort;

    /**
     * 模块logo图片位置
     */
    @Excel(name = "模块logo图片位置")
    private String moduleLogoAddress;

    /**
     * 模块单调登录地址
     */
    @Excel(name = "模块单调登录地址")
    private String moduleSsoAddress;

    /**
     * 模块待办地址
     */
    @Excel(name = "模块待办地址")
    private String moduleTodoAddress;

    /**
     * 是否删除标志位：0-正常，1-删除
     */
    //@Excel(name = "是否删除标志位：0-正常，1-删除")
    private Long isDel;

    /**
     * 序号
     */
    private Long no;

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleShortName(String moduleShortName) {
        this.moduleShortName = moduleShortName;
    }

    public String getModuleShortName() {
        return moduleShortName;
    }

    public void setModuleShortNameEn(String moduleShortNameEn) {
        this.moduleShortNameEn = moduleShortNameEn;
    }

    public String getModuleShortNameEn() {
        return moduleShortNameEn;
    }

    public String getModuleProtocol() {
        return moduleProtocol;
    }

    public void setModuleProtocol(String moduleProtocol) {
        this.moduleProtocol = moduleProtocol;
    }

    public void setModuleAddress(String moduleAddress) {
        this.moduleAddress = moduleAddress;
    }

    public String getModuleAddress() {
        return moduleAddress;
    }

    public void setModulePort(Long modulePort) {
        this.modulePort = modulePort;
    }

    public Long getModulePort() {
        return modulePort;
    }

    public void setModuleLogoAddress(String moduleLogoAddress) {
        this.moduleLogoAddress = moduleLogoAddress;
    }

    public String getModuleLogoAddress() {
        return moduleLogoAddress;
    }

    public void setModuleSsoAddress(String moduleSsoAddress) {
        this.moduleSsoAddress = moduleSsoAddress;
    }

    public String getModuleSsoAddress() {
        return moduleSsoAddress;
    }

    public void setModuleTodoAddress(String moduleTodoAddress) {
        this.moduleTodoAddress = moduleTodoAddress;
    }

    public String getModuleTodoAddress() {
        return moduleTodoAddress;
    }

    public void setIsDel(Long isDel) {
        this.isDel = isDel;
    }

    public Long getIsDel() {
        return isDel;
    }

    public Long getNo() {
        return no;
    }

    public void setNo(Long no) {
        this.no = no;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("moduleId", getModuleId())
                .append("moduleName", getModuleName())
                .append("moduleShortName", getModuleShortName())
                .append("moduleShortNameEn", getModuleShortNameEn())
                .append("moduleAddress", getModuleAddress())
                .append("modulePort", getModulePort())
                .append("moduleLogoAddress", getModuleLogoAddress())
                .append("moduleSsoAddress", getModuleSsoAddress())
                .append("moduleTodoAddress", getModuleTodoAddress())
                .append("remark", getRemark())
                .append("no", getNo())
                .append("isDel", getIsDel())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}
