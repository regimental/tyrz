package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 模块用户对象 biz_user
 * 
 * @author ruoyi
 * @date 2020-11-12
 */
public class BizUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 用户编号 */
    private String userId;

    /** 用户登录账号 */
    @Excel(name = "用户登录账号")
    private String userLogonname;

    /** 用户名称 */
    @Excel(name = "用户名称")
    private String userDisplayname;

    /** 用户密码 */
    @Excel(name = "用户密码")
    private String userPassword;

    /** 是否删除标志位：0-正常，1-删除 */
    @Excel(name = "是否删除标志位：0-正常，1-删除")
    private Long isDel;

    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setUserLogonname(String userLogonname) 
    {
        this.userLogonname = userLogonname;
    }

    public String getUserLogonname() 
    {
        return userLogonname;
    }
    public void setUserDisplayname(String userDisplayname) 
    {
        this.userDisplayname = userDisplayname;
    }

    public String getUserDisplayname() 
    {
        return userDisplayname;
    }
    public void setUserPassword(String userPassword) 
    {
        this.userPassword = userPassword;
    }

    public String getUserPassword() 
    {
        return userPassword;
    }
    public void setIsDel(Long isDel) 
    {
        this.isDel = isDel;
    }

    public Long getIsDel() 
    {
        return isDel;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("userId", getUserId())
            .append("userLogonname", getUserLogonname())
            .append("userDisplayname", getUserDisplayname())
            .append("userPassword", getUserPassword())
            .append("remark", getRemark())
            .append("isDel", getIsDel())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
