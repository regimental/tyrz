package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 模块用户对象 biz_user_module
 *
 * @author ruoyi
 * @date 2020-11-12
 */
public class BizUserModule extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;

    /**
     * 用户编号
     */
    //@Excel(name = "用户编号")
    private String userId;

    /**
     * 模块编号
     */
    //@Excel(name = "模块编号")
    private String moduleId;

    /**
     * 应用用户名称
     */
    @Excel(name = "应用账户名称")
    private String appUserDisplayname;

    /**
     * 应用账户名称
     */
    @Excel(name = "应用登录账户")
    private String appUserLogonname;

    /**
     * 应用用户密码
     */
    @Excel(name = "应用登录密码")
    private String appUserPassword;

    /**
     * 用户所在模块登录账号
     */
    @Excel(name = "模块登录账号")
    private String userLogonname;

    /**
     * 用户所在模块登录密码
     */
    @Excel(name = "模块登录密码")
    private String userPassword;

    /**
     * 是否删除标志位：0-正常，1-删除
     */
    //@Excel(name = "是否删除标志位：0-正常，1-删除")
    private Long isDel;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setUserLogonname(String userLogonname) {
        this.userLogonname = userLogonname;
    }

    public String getUserLogonname() {
        return userLogonname;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setIsDel(Long isDel) {
        this.isDel = isDel;
    }

    public Long getIsDel() {
        return isDel;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("userId", getUserId())
                .append("moduleId", getModuleId())
                .append("userLogonname", getUserLogonname())
                .append("userPassword", getUserPassword())
                .append("remark", getRemark())
                .append("isDel", getIsDel())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .toString();
    }

    public String getAppUserLogonname() {
        return appUserLogonname;
    }

    public void setAppUserLogonname(String appUserLogonname) {
        this.appUserLogonname = appUserLogonname;
    }

    public String getAppUserDisplayname() {
        return appUserDisplayname;
    }

    public void setAppUserDisplayname(String appUserDisplayname) {
        this.appUserDisplayname = appUserDisplayname;
    }

    public String getAppUserPassword() {
        return appUserPassword;
    }

    public void setAppUserPassword(String appUserPassword) {
        this.appUserPassword = appUserPassword;
    }
}
