package com.ruoyi.system.domain;

public class UserModule {
    /**
     * 模块编号
     */
    private String moduleId;

    /**
     * 模块名称
     */
    private String moduleName;

    /**
     * 模块简称
     */
    private String moduleShortName;

    /**
     * 模块英文简称
     */
    private String moduleShortNameEn;

    /**
     * 模块英文简称
     */
    private String moduleProtocol;

    /**
     * 模块地址：ip或者域名
     */
    private String moduleAddress;

    /**
     * 模块端口
     */
    private Long modulePort;

    /**
     * 模块logo图片位置
     */
    private String moduleLogoAddress;

    /**
     * 模块单调登录地址
     */
    private String moduleSsoAddress;

    /**
     * 模块待办地址
     */
    private String moduleTodoAddress;

    /**
     * 序号
     */
    private Long no;

    /**
     * 用户所在模块登录账号
     */
    private String userLogonname;

    /**
     * 用户所在模块登录密码
     */
    private String userPassword;

    private String appUserDisplayname;

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getModuleShortName() {
        return moduleShortName;
    }

    public void setModuleShortName(String moduleShortName) {
        this.moduleShortName = moduleShortName;
    }

    public String getModuleShortNameEn() {
        return moduleShortNameEn;
    }

    public void setModuleShortNameEn(String moduleShortNameEn) {
        this.moduleShortNameEn = moduleShortNameEn;
    }

    public String getModuleProtocol() {
        return moduleProtocol;
    }

    public void setModuleProtocol(String moduleProtocol) {
        this.moduleProtocol = moduleProtocol;
    }

    public String getModuleAddress() {
        return moduleAddress;
    }

    public void setModuleAddress(String moduleAddress) {
        this.moduleAddress = moduleAddress;
    }

    public Long getModulePort() {
        return modulePort;
    }

    public void setModulePort(Long modulePort) {
        this.modulePort = modulePort;
    }

    public String getModuleLogoAddress() {
        return moduleLogoAddress;
    }

    public void setModuleLogoAddress(String moduleLogoAddress) {
        this.moduleLogoAddress = moduleLogoAddress;
    }

    public String getModuleSsoAddress() {
        return moduleSsoAddress;
    }

    public void setModuleSsoAddress(String moduleSsoAddress) {
        this.moduleSsoAddress = moduleSsoAddress;
    }

    public String getModuleTodoAddress() {
        return moduleTodoAddress;
    }

    public void setModuleTodoAddress(String moduleTodoAddress) {
        this.moduleTodoAddress = moduleTodoAddress;
    }

    public Long getNo() {
        return no;
    }

    public void setNo(Long no) {
        this.no = no;
    }

    public String getUserLogonname() {
        return userLogonname;
    }

    public void setUserLogonname(String userLogonname) {
        this.userLogonname = userLogonname;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getAppUserDisplayname() {
        return appUserDisplayname;
    }

    public void setAppUserDisplayname(String appUserDisplayname) {
        this.appUserDisplayname = appUserDisplayname;
    }
}
