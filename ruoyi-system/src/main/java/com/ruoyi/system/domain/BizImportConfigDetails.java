package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 导入配置明细对象 biz_import_config_details
 * 
 * @author ruoyi
 * @date 2021-01-15
 */
public class BizImportConfigDetails extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 父键 */
    @Excel(name = "父键")
    private String parentId;

    /** 单元格数据类型，小数、整数、文本、日期 */
    @Excel(name = "单元格数据类型，小数、整数、文本、日期")
    private Long cellType;

    /** 所在列，以英文逗号分割 */
    @Excel(name = "所在列，以英文逗号分割")
    private String columns;

    /** 起始行 */
    @Excel(name = "起始行")
    private Long startRow;

    /** 起始列 */
    @Excel(name = "起始列")
    private Long startColumn;

    /** 是否必填项，1：必填，0：非必填 */
    @Excel(name = "是否必填项，1：必填，0：非必填")
    private Long required;

    /** 正则表达式 */
    @Excel(name = "正则表达式")
    private String regExpress;

    /** 最大值 */
    @Excel(name = "最大值")
    private Long maxValue;

    /** 最小值 */
    @Excel(name = "最小值")
    private Long minValue;

    /** 小数位数上限 */
    @Excel(name = "小数位数上限")
    private Long floatLimit;

    /** 日期格式 */
    @Excel(name = "日期格式")
    private String dateFormat;

    /** 是否枚举，1：是，0：否 */
    @Excel(name = "是否枚举，1：是，0：否")
    private Long isEnum;

    /** 枚举列表，以英文逗号分隔 */
    @Excel(name = "枚举列表，以英文逗号分隔")
    private String textEnum;

    /** 工作簿索引列表，以英文逗号分割 */
    @Excel(name = "工作簿索引列表，以英文逗号分割")
    private String sheetIndexes;

    /** 扩展字段 */
    @Excel(name = "扩展字段")
    private String extend1;

    /** 扩展字段 */
    @Excel(name = "扩展字段")
    private String extend2;

    /** 扩展字段 */
    @Excel(name = "扩展字段")
    private String extend3;

    /** 是否删除标志位：0-正常，1-删除 */
    @Excel(name = "是否删除标志位：0-正常，1-删除")
    private Long isDel;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setParentId(String parentId) 
    {
        this.parentId = parentId;
    }

    public String getParentId() 
    {
        return parentId;
    }
    public void setCellType(Long cellType) 
    {
        this.cellType = cellType;
    }

    public Long getCellType() 
    {
        return cellType;
    }
    public void setColumns(String columns) 
    {
        this.columns = columns;
    }

    public String getColumns() 
    {
        return columns;
    }
    public void setStartRow(Long startRow) 
    {
        this.startRow = startRow;
    }

    public Long getStartRow() 
    {
        return startRow;
    }
    public void setStartColumn(Long startColumn) 
    {
        this.startColumn = startColumn;
    }

    public Long getStartColumn() 
    {
        return startColumn;
    }
    public void setRequired(Long required) 
    {
        this.required = required;
    }

    public Long getRequired() 
    {
        return required;
    }
    public void setRegExpress(String regExpress) 
    {
        this.regExpress = regExpress;
    }

    public String getRegExpress() 
    {
        return regExpress;
    }
    public void setMaxValue(Long maxValue) 
    {
        this.maxValue = maxValue;
    }

    public Long getMaxValue() 
    {
        return maxValue;
    }
    public void setMinValue(Long minValue) 
    {
        this.minValue = minValue;
    }

    public Long getMinValue() 
    {
        return minValue;
    }
    public void setFloatLimit(Long floatLimit) 
    {
        this.floatLimit = floatLimit;
    }

    public Long getFloatLimit() 
    {
        return floatLimit;
    }
    public void setDateFormat(String dateFormat) 
    {
        this.dateFormat = dateFormat;
    }

    public String getDateFormat() 
    {
        return dateFormat;
    }
    public void setIsEnum(Long isEnum) 
    {
        this.isEnum = isEnum;
    }

    public Long getIsEnum() 
    {
        return isEnum;
    }
    public void setTextEnum(String textEnum) 
    {
        this.textEnum = textEnum;
    }

    public String getTextEnum() 
    {
        return textEnum;
    }
    public void setSheetIndexes(String sheetIndexes) 
    {
        this.sheetIndexes = sheetIndexes;
    }

    public String getSheetIndexes() 
    {
        return sheetIndexes;
    }
    public void setExtend1(String extend1) 
    {
        this.extend1 = extend1;
    }

    public String getExtend1() 
    {
        return extend1;
    }
    public void setExtend2(String extend2) 
    {
        this.extend2 = extend2;
    }

    public String getExtend2() 
    {
        return extend2;
    }
    public void setExtend3(String extend3) 
    {
        this.extend3 = extend3;
    }

    public String getExtend3() 
    {
        return extend3;
    }
    public void setIsDel(Long isDel) 
    {
        this.isDel = isDel;
    }

    public Long getIsDel() 
    {
        return isDel;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("parentId", getParentId())
            .append("cellType", getCellType())
            .append("columns", getColumns())
            .append("startRow", getStartRow())
            .append("startColumn", getStartColumn())
            .append("required", getRequired())
            .append("regExpress", getRegExpress())
            .append("maxValue", getMaxValue())
            .append("minValue", getMinValue())
            .append("floatLimit", getFloatLimit())
            .append("dateFormat", getDateFormat())
            .append("isEnum", getIsEnum())
            .append("textEnum", getTextEnum())
            .append("sheetIndexes", getSheetIndexes())
            .append("extend1", getExtend1())
            .append("extend2", getExtend2())
            .append("extend3", getExtend3())
            .append("remark", getRemark())
            .append("isDel", getIsDel())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
