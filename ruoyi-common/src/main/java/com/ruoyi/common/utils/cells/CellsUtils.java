package com.ruoyi.common.utils.cells;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.bean.ImportConfigDetails;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.FileInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

/**
 * cells工具类
 */
public class CellsUtils {
    /**
     * 判断数字
     */
    public static boolean isNumber(String str) {
        if (StringUtils.isEmpty(str)) {
            return false;
        }
        Pattern pattern = Pattern.compile("^(-|\\+)?\\d+(\\.\\d+)?$");
        return pattern.matcher(str).matches();
    }

    /**
     * 判断整数
     */
    public static boolean isInteger(String str) {
        if (StringUtils.isEmpty(str)) {
            return false;
        }
        Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
        return pattern.matcher(str).matches();
    }

    /**
     * 判断浮点数
     */
    public static boolean isDouble(String str) {
        if (StringUtils.isEmpty(str)) {
            return false;
        }
        Pattern pattern = Pattern.compile("[+-]?[0-9]+\\.([0-9]+)"); // 之前这里正则表达式错误，现更正
        return pattern.matcher(str).matches();
    }

    /**
     * 判断日期
     */
    public static boolean isDate(String str) {
        boolean result = false;
        try {
            Date.parse(str);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 判断必填项
     */
    public static boolean isNull(Object obj) {
        boolean result = true;

        if (obj != null && !StringUtils.isEmpty(obj.toString())) {
            result = false;
        }

        return result;
    }

    /**
     * 设置备注
     *
     * @param ws     工作簿
     * @param row    行
     * @param column 列
     * @param note   备注
     */
    public static void setComment(HSSFSheet ws, int row, int column, String note) {
        //创建绘图对象
        HSSFPatriarch p = ws.createDrawingPatriarch();
        HSSFComment comment = p.createComment(new HSSFClientAnchor(0, 0, 0, 0, (short) 3, 3, (short) 5, 6));
        //输入批注信息
        comment.setString(new HSSFRichTextString(comment.getString() + "\n" + note));
        //将批注添加到单元格对象中
        ws.getRow(row).getCell(column).setCellComment(comment);
        //CellsUtils.setWarningStyle(ws, row, column);
    }

    /**
     * 设置提示颜色
     *
     * @param ws     工作簿
     * @param row    行
     * @param column 列
     */
    public static void setWarningStyle(HSSFSheet ws, int row, int column) {
        HSSFCellStyle style = ws.getRow(row).getCell(column).getCellStyle();
        style.setFillForegroundColor((short) 13);// 设置背景色
        //style.setFillBackgroundColor((short) 13);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        ws.getRow(row).getCell(column).setCellStyle(style);
    }

    private static Object getCellValue(Cell cell) {
        Object value = null;

        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                value = cell.getRichStringCellValue().getString();
                break;
            case Cell.CELL_TYPE_NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    value = cell.getDateCellValue();
                } else {
                    value = cell.getNumericCellValue();
                }
                break;
            case Cell.CELL_TYPE_BOOLEAN:
                value = cell.getBooleanCellValue();
                break;
            case Cell.CELL_TYPE_FORMULA:
                value = cell.getCellFormula();
                break;
            default:
                break;
        }

        return value;
    }

    /**
     * 文件检测
     *
     * @param importConfigDetails 导入配置
     * @param filePath            excel文件路径
     * @return 单元格值
     */
    public static int errorsCheck(List<ImportConfigDetails> importConfigDetails, String filePath, String newFile) throws Exception {
        int error = 0;

        //File excel = new File(filePath);
        //if (!excel.exists()) {
        //throw new Exception("excel文件不存在");
        //}

        FileInputStream inputStream = new FileInputStream(filePath);
        HSSFWorkbook wb = new HSSFWorkbook(inputStream);

        for (ImportConfigDetails config : importConfigDetails) {
            switch (config.cellType) {
                case FLOAT:
                    error += CellsUtils.checkFloatCells(wb, config);
                    break;
                case INTEGER:
                    error += CellsUtils.checkIntegerCells(wb, config);
                    break;
                case TEXT:
                    error += CellsUtils.checkTextCells(wb, config);
                    break;
                case DATE:
                    error += CellsUtils.checkDateCells(wb, config);
                    break;
                default:
                    break;
            }
        }

        wb.write(new File(newFile));
        wb.close();
        inputStream.close();

        return error;
    }

    private static int checkFloatCells(HSSFWorkbook wb, ImportConfigDetails importConfigDetails) throws Exception {
        int error = 0;

        //检测指定工作簿
        if (importConfigDetails.getSheetIndexes() != null && importConfigDetails.getSheetIndexes().length > 0) {
            for (int index : importConfigDetails.getSheetIndexes()) {
                HSSFSheet ws = wb.getSheetAt(index);
                error += CellsUtils.checkFloatCells(ws, importConfigDetails);
            }
        }
        //未指定，检测首个工作簿
        else {
            HSSFSheet ws = wb.getSheetAt(0);
            error += CellsUtils.checkFloatCells(ws, importConfigDetails);
        }

        return error;
    }

    private static int checkFloatCells(HSSFSheet ws, ImportConfigDetails importConfigDetails) {
        int error = 0;

        int row = importConfigDetails.getStartRow() > 0 ? importConfigDetails.getStartRow() : 0;
        int column = importConfigDetails.getStartColumn() > 0 ? importConfigDetails.getStartColumn() : 0;

        for (; row < ws.getLastRowNum(); row++) {
            for (; column < ws.getRow(0).getLastCellNum(); column++) {
                if (importConfigDetails.getColumns().stream().count() > 0) {
                    if (importConfigDetails.getColumns().contains(column)) {

                        Object obj = CellsUtils.getCellValue(ws.getRow(row).getCell(column));

                        //必填项校验
                        if (importConfigDetails.isRequired() && CellsUtils.isNull(obj)) {
                            error++;
                            CellsUtils.setComment(ws, row, column, "必填项");
                        }

                        if (obj != null) {
                            String value = obj.toString();
                            //检测值是否为数字
                            if (!CellsUtils.isNumber(value)) {
                                error++;
                                CellsUtils.setComment(ws, row, column, "必须为数字");
                            }
                            //检测值是否为小数
                            if (CellsUtils.isNumber(value) && !CellsUtils.isDouble(value)) {
                                error++;
                                CellsUtils.setComment(ws, row, column, "必须为小数");
                            }
                            //检测值是否大于最大值
                            if (CellsUtils.isNumber(value) && importConfigDetails.getMaxValue() != null && (Convert.toDouble(value) > importConfigDetails.getMaxValue())) {
                                error++;
                                CellsUtils.setComment(ws, row, column, "不能大于最大值");
                            }
                            //检测值是否小于最小值
                            if (CellsUtils.isNumber(value) && importConfigDetails.getMinValue() != null && (Convert.toDouble(value) < importConfigDetails.getMinValue())) {
                                error++;
                                CellsUtils.setComment(ws, row, column, "不能小于最小值");
                            }
                            //检测小数位是否高于上限
                            if (CellsUtils.isNumber(value) && CellsUtils.isDouble(value) && importConfigDetails.getFloatLimit() != null && value.split(".")[1].length() > importConfigDetails.getFloatLimit()) {
                                error++;
                                CellsUtils.setComment(ws, row, column, "小数位高于上限");
                            }
                        }
                    }
                }
            }
        }
        return error;
    }

    private static int checkIntegerCells(HSSFWorkbook wb, ImportConfigDetails importConfigDetails) throws Exception {
        int error = 0;

        //检测指定工作簿
        if (importConfigDetails.getSheetIndexes() != null && importConfigDetails.getSheetIndexes().length > 0) {
            for (int index : importConfigDetails.getSheetIndexes()) {
                HSSFSheet ws = wb.getSheetAt(index);
                error += CellsUtils.checkIntegerCells(ws, importConfigDetails);
            }
        }
        //未指定，检测首个工作簿
        else {
            HSSFSheet ws = wb.getSheetAt(0);
            error += CellsUtils.checkIntegerCells(ws, importConfigDetails);
        }

        return error;
    }

    private static int checkIntegerCells(HSSFSheet ws, ImportConfigDetails importConfigDetails) {
        int error = 0;

        int row = importConfigDetails.getStartRow() > 0 ? importConfigDetails.getStartRow() : 0;
        int column = importConfigDetails.getStartColumn() > 0 ? importConfigDetails.getStartColumn() : 0;

        for (; row < ws.getLastRowNum(); row++) {
            for (; column < ws.getRow(row).getLastCellNum(); column++) {
                if (importConfigDetails.getColumns().stream().count() > 0) {
                    if (importConfigDetails.getColumns().contains(column)) {

                        Object obj = CellsUtils.getCellValue(ws.getRow(row).getCell(column));

                        //必填项校验
                        if (importConfigDetails.isRequired() && CellsUtils.isNull(obj)) {
                            error++;
                            CellsUtils.setComment(ws, row, column, "必填项");
                        }

                        if (obj != null) {
                            String value = obj.toString();
                            //检测值是否为数字
                            if (!CellsUtils.isNumber(value)) {
                                error++;
                                CellsUtils.setComment(ws, row, column, "必须为数字");
                            }
                            //检测值是否为整数
                            if (CellsUtils.isNumber(value) && !CellsUtils.isInteger(value)) {
                                error++;
                                CellsUtils.setComment(ws, row, column, "必须为整数");
                            }
                            //检测值是否大于最大值
                            if (CellsUtils.isNumber(value) && importConfigDetails.getMaxValue() != null && (Convert.toDouble(value) > importConfigDetails.getMaxValue())) {
                                error++;
                                CellsUtils.setComment(ws, row, column, "不能大于最大值");
                            }
                            //检测值是否小于最小值
                            if (CellsUtils.isNumber(value) && importConfigDetails.getMinValue() != null && (Convert.toDouble(value) < importConfigDetails.getMinValue())) {
                                error++;
                                CellsUtils.setComment(ws, row, column, "不能小于最小值");
                            }
                            //检测枚举
                            if (CellsUtils.isNumber(value) && importConfigDetails.isEnum() && importConfigDetails.getTextEnum().length > 0) {
                                if (!Arrays.stream(importConfigDetails.getTextEnum()).findAny().equals(value)) {
                                    error++;
                                    CellsUtils.setComment(ws, row, column, "不符合枚举要求");
                                }
                            }
                        }
                    }
                }
            }
        }
        return error;
    }

    private static int checkTextCells(HSSFWorkbook wb, ImportConfigDetails importConfigDetails) throws Exception {
        int error = 0;

        //检测指定工作簿
        if (importConfigDetails.getSheetIndexes() != null && importConfigDetails.getSheetIndexes().length > 0) {
            for (int index : importConfigDetails.getSheetIndexes()) {
                HSSFSheet ws = wb.getSheetAt(index);
                error += CellsUtils.checkTextCells(ws, importConfigDetails);
            }
        }
        //未指定，检测首个工作簿
        else {
            HSSFSheet ws = wb.getSheetAt(0);
            error += CellsUtils.checkTextCells(ws, importConfigDetails);
        }

        return error;
    }

    private static int checkTextCells(HSSFSheet ws, ImportConfigDetails importConfigDetails) {
        int error = 0;

        int row = importConfigDetails.getStartRow() > 0 ? importConfigDetails.getStartRow() : 0;
        int column = importConfigDetails.getStartColumn() > 0 ? importConfigDetails.getStartColumn() : 0;

        for (; row < ws.getLastRowNum(); row++) {
            for (; column < ws.getRow(row).getLastCellNum(); column++) {
                if (importConfigDetails.getColumns().stream().count() > 0) {
                    if (importConfigDetails.getColumns().contains(column)) {

                        Object obj = CellsUtils.getCellValue(ws.getRow(row).getCell(column));

                        //必填项校验
                        if (importConfigDetails.isRequired() && CellsUtils.isNull(obj)) {
                            error++;
                            CellsUtils.setComment(ws, row, column, "必填项");
                        }

                        if (obj != null) {
                            String value = obj.toString();
                            //检测文本长度是否大于最大值
                            if (importConfigDetails.getMaxValue() != null && (value.length() > importConfigDetails.getMaxValue())) {
                                error++;
                                CellsUtils.setComment(ws, row, column, "文本长度不能大于" + importConfigDetails.getMaxValue());
                            }
                            //检测文本长度是否小于最小值
                            if (importConfigDetails.getMinValue() != null && (value.length() < importConfigDetails.getMinValue())) {
                                error++;
                                CellsUtils.setComment(ws, row, column, "文本长度不能小于" + importConfigDetails.getMinValue());
                            }
                            //检测枚举
                            if (importConfigDetails.isEnum() && importConfigDetails.getTextEnum().length > 0) {
                                if (!Arrays.stream(importConfigDetails.getTextEnum()).findAny().equals(value)) {
                                    error++;
                                    CellsUtils.setComment(ws, row, column, "不符合枚举要求");
                                }
                            }
                        }
                    }
                }
            }
        }
        return error;
    }

    private static int checkDateCells(HSSFWorkbook wb, ImportConfigDetails importConfigDetails) throws Exception {
        int error = 0;

        //检测指定工作簿
        if (importConfigDetails.getSheetIndexes() != null && importConfigDetails.getSheetIndexes().length > 0) {
            for (int index : importConfigDetails.getSheetIndexes()) {
                HSSFSheet ws = wb.getSheetAt(index);
                error += CellsUtils.checkDateCells(ws, importConfigDetails);
            }
        }
        //未指定，检测首个工作簿
        else {
            HSSFSheet ws = wb.getSheetAt(0);
            error += CellsUtils.checkDateCells(ws, importConfigDetails);
        }

        return error;
    }

    private static int checkDateCells(HSSFSheet ws, ImportConfigDetails importConfigDetails) {
        int error = 0;

        int row = importConfigDetails.getStartRow() > 0 ? importConfigDetails.getStartRow() : 0;
        int column = importConfigDetails.getStartColumn() > 0 ? importConfigDetails.getStartColumn() : 0;

        for (; row < ws.getLastRowNum(); row++) {
            for (; column < ws.getRow(row).getLastCellNum(); column++) {
                if (importConfigDetails.getColumns().stream().count() > 0) {
                    if (importConfigDetails.getColumns().contains(column)) {

                        Object obj = CellsUtils.getCellValue(ws.getRow(row).getCell(column));

                        //必填项校验
                        if (importConfigDetails.isRequired() && CellsUtils.isNull(obj)) {
                            error++;
                            CellsUtils.setComment(ws, row, column, "必填项");
                        }

                        if (obj != null) {
                            String value = obj.toString();
                            //检测是否为日期
                            if (!CellsUtils.isDate(value)) {
                                error++;
                                CellsUtils.setComment(ws, row, column, "日期格式错误");
                            }
                            //检测是否满足格式
                            if (!StringUtils.isEmpty(importConfigDetails.dateFormat)) {
                                SimpleDateFormat format = new SimpleDateFormat(importConfigDetails.dateFormat);
                                try {
                                    Date date = format.parse(value);
                                } catch (ParseException e) {
                                    error++;
                                    CellsUtils.setComment(ws, row, column, "日期格式错误");
                                }
                            }
                            //判断正则表达式
                            if (!StringUtils.isEmpty(importConfigDetails.regExpress) && !value.matches(importConfigDetails.regExpress)) {
                                error++;
                                CellsUtils.setComment(ws, row, column, "不满足正则表达式");
                            }
                        }
                    }
                }
            }
        }

        return error;
    }
}
