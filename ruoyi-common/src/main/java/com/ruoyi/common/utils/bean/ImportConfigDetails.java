package com.ruoyi.common.utils.bean;

import com.ruoyi.common.enums.CellType;

import java.util.ArrayList;

/**
 * 单元格模型
 */
public class ImportConfigDetails {
    /**
     * 数据类型
     */
    public CellType cellType;

    /**
     * 列族
     */
    public ArrayList<Integer> columns;

    /**
     * 起始行
     */
    public int startRow;

    /**
     * 起始列
     */
    public int startColumn;

    /**
     * 起始列
     */
    public boolean required;

    /**
     * 正则表达式
     */
    public String regExpress;

    /**
     * 最大值
     */
    public Double maxValue;

    /**
     * 最小值
     */
    public Double minValue;

    /**
     * 小数位上限
     */
    public Integer floatLimit;

    /**
     * 日期格式
     */
    public String dateFormat;

    /**
     * 是否是枚举
     */
    public boolean isEnum;

    /**
     * 枚举列表
     */
    public Object[] textEnum;

    /**
     * 工作簿索引集合
     */
    public int[] sheetIndexes;

    /**
     * 预留字段
     */
    public String extend1;

    /**
     * 预留字段
     */
    public String extend2;

    /**
     * 预留字段
     */
    public String extend3;

    public CellType getCellType() {
        return cellType;
    }

    public void setCellType(CellType cellType) {
        this.cellType = cellType;
    }

    public ArrayList<Integer> getColumns() {
        return columns;
    }

    public void setColumns(ArrayList<Integer> columns) {
        this.columns = columns;
    }

    public int getStartRow() {
        return startRow;
    }

    public void setStartRow(int startRow) {
        this.startRow = startRow;
    }

    public int getStartColumn() {
        return startColumn;
    }

    public void setStartColumn(int startColumn) {
        this.startColumn = startColumn;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getRegExpress() {
        return regExpress;
    }

    public void setRegExpress(String regExpress) {
        this.regExpress = regExpress;
    }

    public Double getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(double maxValue) {
        this.maxValue = maxValue;
    }

    public Double getMinValue() {
        return minValue;
    }

    public void setMinValue(double minValue) {
        this.minValue = minValue;
    }

    public Integer getFloatLimit() {
        return floatLimit;
    }

    public void setFloatLimit(int floatLimit) {
        this.floatLimit = floatLimit;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public boolean isEnum() {
        return isEnum;
    }

    public void setEnum(boolean anEnum) {
        isEnum = anEnum;
    }

    public Object[] getTextEnum() {
        return textEnum;
    }

    public void setTextEnum(Object[] textEnum) {
        this.textEnum = textEnum;
    }

    public int[] getSheetIndexes() {
        return sheetIndexes;
    }

    public void setSheetIndexes(int[] sheetIndexes) {
        this.sheetIndexes = sheetIndexes;
    }

    public String getExtend1() {
        return extend1;
    }

    public void setExtend1(String extend1) {
        this.extend1 = extend1;
    }

    public String getExtend2() {
        return extend2;
    }

    public void setExtend2(String extend2) {
        this.extend2 = extend2;
    }

    public String getExtend3() {
        return extend3;
    }

    public void setExtend3(String extend3) {
        this.extend3 = extend3;
    }
}
