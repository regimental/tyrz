package com.ruoyi.common.enums;

/**
 * 单元格数据类型
 */
public enum CellType {
    /**
     * 小数
     */
    FLOAT,

    /**
     * 整数
     */
    INTEGER,

    /**
     * 文本
     */
    TEXT,

    /**
     * 日期
     */
    DATE;

    public static CellType convert(int value) {
        switch (value) {
            case 0:
                return CellType.FLOAT;
            case 1:
                return CellType.INTEGER;
            case 2:
                return CellType.TEXT;
            case 3:
                return CellType.DATE;
            default:
                return null;
        }
    }
}
