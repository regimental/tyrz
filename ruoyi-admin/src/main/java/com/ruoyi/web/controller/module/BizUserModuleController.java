package com.ruoyi.web.controller.module;

import java.util.List;

import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.system.domain.BizUserModule;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.service.IBizUserModuleService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 模块用户Controller
 *
 * @author ruoyi
 * @date 2020-11-12
 */
@RestController
@RequestMapping("/system/user/module")
public class BizUserModuleController extends BaseController {
    @Autowired
    private IBizUserModuleService bizUserModuleService;

    /**
     * 查询模块用户列表
     */
    @PreAuthorize("@ss.hasPermi('system:user:module:list')")
    @Log(title = "模块用户", businessType = BusinessType.QUERY)
    @GetMapping("/list")
    public TableDataInfo list(BizUserModule bizUserModule) throws Exception {
        startPage();
        List<BizUserModule> list = bizUserModuleService.selectBizUserModuleList(bizUserModule);
        return getDataTable(list);
    }

    @Log(title = "模块用户", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('system:user:module:grant')")
    @GetMapping("/exportTemplate")
    public AjaxResult importTemplate() {
        ExcelUtil<BizUserModule> util = new ExcelUtil<BizUserModule>(BizUserModule.class);
        return util.importTemplateExcel("用户数据");
    }

    @Log(title = "模块用户", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('system:user:module:grant')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, String moduleId) throws Exception {
        ExcelUtil<BizUserModule> util = new ExcelUtil<BizUserModule>(BizUserModule.class);
        List<BizUserModule> bizUserModuleList = util.importExcel(file.getInputStream());
        if (bizUserModuleList.size() <= 0) {
            return AjaxResult.error("excel数据为空");
        }
        //文件没有检验
        StringBuilder msg = bizUserModuleService.importData(bizUserModuleList, moduleId);
        if (msg.toString().length() <= 0) {
            return new AjaxResult(HttpStatus.SUCCESS, "导入成功");
        }
        return new AjaxResult(HttpStatus.DUPLICATES, msg.toString());
    }

    /**
     * 导出模块用户列表
     */
    @PreAuthorize("@ss.hasPermi('system:user:module:export')")
    @Log(title = "模块用户", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BizUserModule bizUserModule) {
        List<BizUserModule> list = bizUserModuleService.selectBizUserModuleList(bizUserModule);
        ExcelUtil<BizUserModule> util = new ExcelUtil<BizUserModule>(BizUserModule.class);
        return util.exportExcel(list, "用户列表");
    }

    /**
     * 获取模块用户详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:user:module:query')")
    @Log(title = "模块用户", businessType = BusinessType.QUERY_ENTITY)
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) throws Exception {
        return AjaxResult.success(bizUserModuleService.selectBizUserModuleById(id));
    }

    /**
     * 新增模块用户
     */
    @PreAuthorize("@ss.hasPermi('system:user:module:add')")
    @Log(title = "模块用户", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BizUserModule bizUserModule) throws Exception {
        int rows = bizUserModuleService.insertBizUserModule(bizUserModule);
        if (rows > 0) {
            return new AjaxResult(HttpStatus.SUCCESS, "操作成功");
        }
        return new AjaxResult(HttpStatus.DUPLICATES, "用户账号已存在");
    }

    /**
     * 新增用户授权
     */
    @PreAuthorize("@ss.hasPermi('system:user:module:grant')")
    @Log(title = "模块用户", businessType = BusinessType.INSERT)
    @PostMapping("/addGrant")
    public AjaxResult addGrant(@RequestBody BizUserModule bizUserModule) throws Exception {
        int rows = bizUserModuleService.addGrant(bizUserModule);
        if (rows > 0) {
            return new AjaxResult(HttpStatus.SUCCESS, "操作成功");
        }
        return new AjaxResult(HttpStatus.DUPLICATES, "用户账号已存在");
    }

    /**
     * 修改模块用户
     */
    @PreAuthorize("@ss.hasPermi('system:user:module:edit')")
    @Log(title = "模块用户", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BizUserModule bizUserModule) throws Exception {
        int rows = bizUserModuleService.updateBizUserModule(bizUserModule);
        if (rows > 0) {
            return new AjaxResult(HttpStatus.SUCCESS, "操作成功");
        }
        return new AjaxResult(HttpStatus.DUPLICATES, "用户账号已存在");
    }

    /**
     * 删除模块用户
     */
    @PreAuthorize("@ss.hasPermi('system:user:module:remove')")
    @Log(title = "模块用户", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(bizUserModuleService.deleteBizUserModuleByIds(ids));
    }
}
