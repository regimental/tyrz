package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.SysAddressBook;
import com.ruoyi.system.service.ISysAddressBookService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 通讯录 信息操作处理
 *
 * @author pmj
 */
@Api("通讯录管理")
@RestController
@RequestMapping("/system/address/book")
public class SysAddressBookController extends BaseController {

    @Autowired
    private ISysAddressBookService addressService;

    /**
     * 获取通讯录列表
     */
    @PreAuthorize("@ss.hasPermi('system:addressBook:list')")
    @Log(title = "通讯录", businessType = BusinessType.QUERY)
    @GetMapping("/list")
    public TableDataInfo list(SysAddressBook addressBook)
    {
        startPage();
        addressBook.setBelongTo(SecurityUtils.getLoginUser().getUser().getUserId());
        List<SysAddressBook> list = addressService.getAddressBookList(addressBook);
        return getDataTable(list);
    }

    /**
     * 根据通讯录编号获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:addressBook:query')")
    @Log(title = "通讯录", businessType = BusinessType.QUERY_ENTITY)
    @GetMapping(value = "/{addressBookId}")
    public AjaxResult getInfo(@PathVariable Long addressBookId)
    {
        return AjaxResult.success(addressService.getAddressBookById(addressBookId));
    }

    /**
     * 新增通讯录
     */
    @PreAuthorize("@ss.hasPermi('system:addressBook:add')")
    @Log(title = "通讯录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody SysAddressBook addressBook)
    {
        addressBook.setBelongTo(SecurityUtils.getLoginUser().getUser().getUserId());
        addressBook.setCreateBy(SecurityUtils.getUsername());
        return toAjax(addressService.insertAddressBook(addressBook));
    }

    /**
     * 修改通讯录
     */
    @PreAuthorize("@ss.hasPermi('system:addressBook:edit')")
    @Log(title = "通讯录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody SysAddressBook addressBook)
    {
        addressBook.setUpdateBy(SecurityUtils.getUsername());
        return toAjax(addressService.updateAddressBook(addressBook));
    }

    /**
     * 删除通讯录
     */
    @PreAuthorize("@ss.hasPermi('system:addressBook:remove')")
    @Log(title = "通讯录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{addressBookIds}")
    public AjaxResult remove(@PathVariable Long[] addressBookIds)
    {
        return toAjax(addressService.deleteAddressBookByIds(addressBookIds));
    }
}
