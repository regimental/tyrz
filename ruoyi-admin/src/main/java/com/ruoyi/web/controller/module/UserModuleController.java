package com.ruoyi.web.controller.module;

import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginBody;
import com.ruoyi.system.domain.BizUserModule;
import com.ruoyi.system.domain.UserModule;
import com.ruoyi.system.service.IBizUserModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserModuleController {
    @Autowired
    private IBizUserModuleService bizUserModuleService;

    /**
     * 新增用户授权
     */
    @PostMapping("/app/add")
    public AjaxResult add(@RequestBody BizUserModule bizUserModule) throws Exception {
        int rows = bizUserModuleService.addBizUserModule(bizUserModule);
        if (rows > 0) {
            return new AjaxResult(HttpStatus.SUCCESS, "操作成功");
        }
        return new AjaxResult(HttpStatus.DUPLICATES, "用户账号已存在");
    }

    /**
     * 修改模块用户
     */
    @PutMapping("/app/edit")
    public AjaxResult edit(@RequestBody BizUserModule bizUserModule) throws Exception {
        int rows = bizUserModuleService.edit(bizUserModule);
        if (rows > 0) {
            return new AjaxResult(HttpStatus.SUCCESS, "操作成功");
        }
        return new AjaxResult(HttpStatus.NOT_FOUND, "没有记录");
    }

    /**
     * 删除模块用户
     */
    @DeleteMapping("/app/remove")
    public AjaxResult remove(String logonNames, String moduleId) {
        int rows = bizUserModuleService.deleteByLogonName(logonNames.split(","), moduleId);
        if (rows > 0) {
            return new AjaxResult(HttpStatus.SUCCESS, "操作成功");
        }
        return new AjaxResult(HttpStatus.NOT_FOUND, "没有记录");
    }

    /**
     * 模块用户登录
     */
    @PostMapping("/app/login")
    public AjaxResult login(@RequestBody LoginBody loginBody) throws Exception {
        // 生成令牌
        String token = bizUserModuleService.login(loginBody);
        return new AjaxResult(HttpStatus.SUCCESS, "OK", token);
    }

    /**
     * 获取用户模块
     */
    @GetMapping("/app/user/modules/{token}")
    public AjaxResult list(@PathVariable("token") String token) {
        List<UserModule> list = bizUserModuleService.getUserModuleList(token);
        return new AjaxResult(HttpStatus.SUCCESS, "操作成功", list);
    }
}
