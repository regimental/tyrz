package com.ruoyi.web.controller.system;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.enums.CellType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.bean.ImportConfigDetails;
import com.ruoyi.common.utils.cells.CellsUtils;
import com.ruoyi.common.utils.file.FileUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.BizImportConfigDetails;
import com.ruoyi.system.service.IBizImportConfigDetailsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 导入配置明细Controller
 *
 * @author ruoyi
 * @date 2021-01-15
 */
@RestController
@RequestMapping("/system/import/config/details")
public class BizImportConfigDetailsController extends BaseController {
    @Autowired
    private IBizImportConfigDetailsService bizImportConfigDetailsService;

    /**
     * 查询导入配置明细列表
     */
    @PreAuthorize("@ss.hasPermi('system:details:list')")
    @GetMapping("/list")
    public TableDataInfo list(BizImportConfigDetails bizImportConfigDetails) {
        startPage();
        List<BizImportConfigDetails> list = bizImportConfigDetailsService.selectBizImportConfigDetailsList(bizImportConfigDetails);
        return getDataTable(list);
    }

    /**
     * 导出导入配置明细列表
     */
    @PreAuthorize("@ss.hasPermi('system:details:export')")
    @Log(title = "导入配置明细", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BizImportConfigDetails bizImportConfigDetails) {
        List<BizImportConfigDetails> list = bizImportConfigDetailsService.selectBizImportConfigDetailsList(bizImportConfigDetails);
        ExcelUtil<BizImportConfigDetails> util = new ExcelUtil<BizImportConfigDetails>(BizImportConfigDetails.class);
        return util.exportExcel(list, "details");
    }

    /**
     * 获取导入配置明细详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:details:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(bizImportConfigDetailsService.selectBizImportConfigDetailsById(id));
    }

    /**
     * 新增导入配置明细
     */
    @PreAuthorize("@ss.hasPermi('system:details:add')")
    @Log(title = "导入配置明细", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BizImportConfigDetails bizImportConfigDetails) {
        return toAjax(bizImportConfigDetailsService.insertBizImportConfigDetails(bizImportConfigDetails));
    }

    /**
     * 修改导入配置明细
     */
    @PreAuthorize("@ss.hasPermi('system:details:edit')")
    @Log(title = "导入配置明细", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BizImportConfigDetails bizImportConfigDetails) {
        return toAjax(bizImportConfigDetailsService.updateBizImportConfigDetails(bizImportConfigDetails));
    }

    /**
     * 删除导入配置明细
     */
    @PreAuthorize("@ss.hasPermi('system:details:remove')")
    @Log(title = "导入配置明细", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(bizImportConfigDetailsService.deleteBizImportConfigDetailsByIds(ids));
    }

    @Log(title = "导入配置明细", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('system:details:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, String parentId) throws Exception {
        String fileName = UUID.randomUUID().toString();
        String filePath = RuoYiConfig.getDownloadPath() + fileName + "_" + file.getOriginalFilename();
        FileOutputStream ops = new FileOutputStream(filePath);
        ops.write(file.getBytes(), 0, file.getBytes().length);
        ops.close();

        List<BizImportConfigDetails> bizImportConfigDetails = bizImportConfigDetailsService.getListByParentId(parentId);

        List<ImportConfigDetails> importConfigDetails = new ArrayList<>();

        for (BizImportConfigDetails item : bizImportConfigDetails) {

            ImportConfigDetails detail = new ImportConfigDetails();

            detail.setCellType(CellType.convert(Convert.toInt(item.getCellType())));

            if (!StringUtils.isEmpty(item.getColumns())) {
                ArrayList<Integer> list = new ArrayList<>();
                for (String val : item.getColumns().split(",")) {
                    list.add(Integer.parseInt(val));
                }
                detail.setColumns(list);
            }

            if (item.getStartRow() != null) {
                detail.setStartRow(Integer.parseInt(item.getStartRow().toString()));
            }

            if (item.getStartColumn() != null) {
                detail.setStartColumn(Integer.parseInt(item.getStartColumn().toString()));
            }

            if (item.getRequired() != null) {
                detail.setRequired(Boolean.parseBoolean(item.getRequired().toString()));
            }

            detail.setRegExpress(item.getRegExpress());

            if (item.getMaxValue() != null) {
                detail.setMaxValue((double) item.getMaxValue());
            }

            if (item.getMinValue() != null) {
                detail.setMinValue((double) item.getMinValue());
            }

            if (item.getFloatLimit() != null) {
                detail.setFloatLimit(Integer.parseInt(item.getFloatLimit().toString()));
            }

            detail.setDateFormat(item.getDateFormat());

            if (item.getIsEnum() != null) {
                detail.setEnum(Boolean.parseBoolean(item.getIsEnum().toString()));
            }

            if (!StringUtils.isEmpty(item.getTextEnum())) {
                detail.setTextEnum(item.getTextEnum().split(","));
            }

            if (!StringUtils.isEmpty(item.getSheetIndexes())) {
                detail.setSheetIndexes(Arrays.stream(item.getSheetIndexes().split(",")).mapToInt(t -> Integer.parseInt(t)).toArray());
            }

            detail.setExtend1(item.getExtend1());
            detail.setExtend2(item.getExtend2());
            detail.setExtend3(item.getExtend3());

            importConfigDetails.add(detail);
        }

        String newFile = RuoYiConfig.getDownloadPath() + UUID.randomUUID() + "_" + file.getOriginalFilename();

        int errors = CellsUtils.errorsCheck(importConfigDetails, filePath, newFile);

        FileUtils.deleteFile(filePath);

        return new AjaxResult(HttpStatus.SUCCESS, newFile, errors);
    }
}
