package com.ruoyi.web.controller.module;

import java.util.List;

import com.ruoyi.system.domain.BizModule;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.service.IBizModuleService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 模块管理Controller
 *
 * @author ruoyi
 * @date 2020-11-12
 */
@RestController
@RequestMapping("/system/module")
public class BizModuleController extends BaseController {
    @Autowired
    private IBizModuleService bizModuleService;

    /**
     * 查询模块列表
     */
    @PreAuthorize("@ss.hasPermi('system:module:list')")
    @Log(title = "模块管理", businessType = BusinessType.QUERY)
    @GetMapping("/list")
    public TableDataInfo list(BizModule bizModule) {
        startPage();
        List<BizModule> list = bizModuleService.selectBizModuleList(bizModule);
        return getDataTable(list);
    }

    /**
     * 查询模块列表
     */
    @PreAuthorize("@ss.hasPermi('system:module:list')")
    @Log(title = "模块管理", businessType = BusinessType.QUERY)
    @GetMapping("/grant/{userId}")
    public TableDataInfo grantList(@PathVariable String userId) {
        startPage();
        List<BizModule> list = bizModuleService.selectBizModuleGrantList(userId);
        return getDataTable(list);
    }

    /**
     * 导出模块列表
     */
    @PreAuthorize("@ss.hasPermi('system:module:export')")
    @Log(title = "模块管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BizModule bizModule) {
        List<BizModule> list = bizModuleService.selectBizModuleList(bizModule);
        ExcelUtil<BizModule> util = new ExcelUtil<BizModule>(BizModule.class);
        return util.exportExcel(list, "module");
    }

    /**
     * 获取模块详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:module:query')")
    @Log(title = "模块管理", businessType = BusinessType.QUERY_ENTITY)
    @GetMapping(value = "/{moduleId}")
    public AjaxResult getInfo(@PathVariable("moduleId") String moduleId) {
        return AjaxResult.success(bizModuleService.selectBizModuleById(moduleId));
    }

    /**
     * 新增模块
     */
    @PreAuthorize("@ss.hasPermi('system:module:add')")
    @Log(title = "模块管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BizModule bizModule) {
        return toAjax(bizModuleService.insertBizModule(bizModule));
    }

    /**
     * 修改模块
     */
    @PreAuthorize("@ss.hasPermi('system:module:edit')")
    @Log(title = "模块管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BizModule bizModule) {
        return toAjax(bizModuleService.updateBizModule(bizModule));
    }

    /**
     * 删除模块
     */
    @PreAuthorize("@ss.hasPermi('system:module:remove')")
    @Log(title = "模块管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{moduleIds}")
    public AjaxResult remove(@PathVariable String[] moduleIds) {
        return toAjax(bizModuleService.deleteBizModuleByIds(moduleIds));
    }
}
