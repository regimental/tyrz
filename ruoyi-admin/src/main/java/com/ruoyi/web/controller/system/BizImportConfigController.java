package com.ruoyi.web.controller.system;

import java.util.List;

import com.ruoyi.system.service.IBizImportConfigDetailsService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.BizImportConfig;
import com.ruoyi.system.service.IBizImportConfigService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 导入配置Controller
 *
 * @author ruoyi
 * @date 2021-01-15
 */
@RestController
@RequestMapping("/system/import/config")
public class BizImportConfigController extends BaseController {
    @Autowired
    private IBizImportConfigService bizImportConfigService;
    @Autowired
    private IBizImportConfigDetailsService bizImportConfigDetailsService;

    /**
     * 查询导入配置列表
     */
    @PreAuthorize("@ss.hasPermi('system:import:list')")
    @GetMapping("/list")
    public TableDataInfo list(BizImportConfig bizImportConfig) {
        startPage();
        List<BizImportConfig> list = bizImportConfigService.selectBizImportConfigList(bizImportConfig);
        return getDataTable(list);
    }

    /**
     * 导出导入配置列表
     */
    @PreAuthorize("@ss.hasPermi('system:import:export')")
    @Log(title = "导入配置", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BizImportConfig bizImportConfig) {
        List<BizImportConfig> list = bizImportConfigService.selectBizImportConfigList(bizImportConfig);
        ExcelUtil<BizImportConfig> util = new ExcelUtil<BizImportConfig>(BizImportConfig.class);
        return util.exportExcel(list, "config");
    }

    /**
     * 获取导入配置详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:import:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(bizImportConfigService.selectBizImportConfigById(id));
    }

    /**
     * 新增导入配置
     */
    @PreAuthorize("@ss.hasPermi('system:import:add')")
    @Log(title = "导入配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BizImportConfig bizImportConfig) {
        return toAjax(bizImportConfigService.insertBizImportConfig(bizImportConfig));
    }

    /**
     * 修改导入配置
     */
    @PreAuthorize("@ss.hasPermi('system:import:edit')")
    @Log(title = "导入配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BizImportConfig bizImportConfig) throws Exception {
        return toAjax(bizImportConfigService.updateBizImportConfig(bizImportConfig));
    }

    /**
     * 删除导入配置
     */
    @PreAuthorize("@ss.hasPermi('system:import:remove')")
    @Log(title = "导入配置", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(bizImportConfigService.deleteBizImportConfigByIds(ids));
    }
}
